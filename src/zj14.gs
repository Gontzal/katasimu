uses Gee
uses SDL
uses SDLImage
uses SDLGraphics
uses SDLTTF
uses GLib

// Entzun zenbakia eta egokitu kopurua.1-50

class zenbakiak14 : Object
	numero: Etiqueta
	hamarreko: list of Imagen
	iman: CuadradoRedondo
	fin:bool=false
	comando:int=0
	solucion:int=0
	contador:int=1
	salir:Imagen
	continuar:Imagen
	zuzendu:Boton
	
	init 
		hamarreko= new list of Imagen
	def inicio()
		
		sdlk.muere_todo()
		fin=false
		comando=-10
		contador=1
		solucion= Random.int_range(1,5)*10
		hamarreko.clear()
		
		numero=( new Etiqueta(-1,240,300, 0, 0, solucion.to_string()))
		numero.set_ColorControl(100,250,250,255)
		numero.set_TamanoTexto(100)
		numero.Arrastrable=false
		numero.Valor_int=solucion
		numero.izq_pulsado.connect(on_numero)
		
		var et= new Etiqueta(-1,250,100,0,0,"Hamarrekoak ->")
		et.set_TamanoTexto(30)
		et.Arrastrable=false
		
		iman= new CuadradoRedondo(-1,400,280,260,160)
		iman.Arrastrable=false
		iman.FiguraFondo=true
		
		for var i=0 to 10
			hamarreko.add(new Imagen(-1,500,60,dd+"irudiak/erabilgarri/hamarreko.png"))
			hamarreko[i].set_Tamano(50,150)
			hamarreko[i].soltado.connect(on_soltar_hamarreko)
			hamarreko[i].izq_pulsado.connect(on_tomar_hamarreko)
			hamarreko[i].Valor_int=10
		
		salir= new Imagen(1,70,547,dd+"irudiak/erabilgarri/atzera.png")
		salir.set_Tamano(40,40)
		salir.izq_pulsado.connect(on_salir)
		
		continuar= new Imagen(1,120,547,dd+"irudiak/erabilgarri/Repetir.png")
		continuar.set_Tamano(40,40)
		continuar.izq_pulsado.connect(on_continuar)
		
		zuzendu= new Boton(1,320,550,0,0,"Zuzendu")
		zuzendu.izq_pulsado.connect(on_zuzendu)
		
		while not fin
			// toma eventos.
			sdlk.mira_sobre()
			while (SDL.Event.poll (out evento))== 1
				if evento.type == SDL.EventType.QUIT
					fin= true
					comando=-1
					break
				else
					sdlk.toma_eventos(evento)
			//pintar el fondo
			pinta_fondo()		
			// Realiza los cambios de juego necesarios
			sdlk.update_control()
			// Pinta los controles
			sdlk.pintar()
			SDL.Timer.delay(50)
		//SDL.Timer.delay(500)
		P.continua(comando)
	def pinta_fondo()
		Rectangle.fill_rgba(screen, 0, 0, sdlk.Ancho, sdlk.Alto, 255,210,151,255)
		dr:Rect= new Rect
		dr.x=10; dr.y=sdlk.Alto-60; dr.w=sdlk.Ancho-20; dr.h=55
		sdlk.Rectangulo_redondo(screen,dr,10,100,100,100,255)
		
		dr.x=10; dr.y=10; dr.w=sdlk.Ancho-20; dr.h=sdlk.Alto-80
		sdlk.Rectangulo_redondo(screen,dr,10,150,100,150,255)
	def on_soltar_hamarreko (c:Control)
		if sdlk.colision_mascara(iman,c) 
			if iman.Valor_int<50
				iman.Valor_int+=c.Valor_int
				c.Valor_bool=true
				sonidos.play("blub")
				sonidos.play("archivo",dd+"soinuak/zenbakiak/"+tostring(3,iman.Valor_int)+".ogg")
				c.set_Posicion_y((iman.dr.y+iman.dr.h/2)-(c.dr.h/2))
				c.set_Posicion_x(iman.dr.x+iman.Valor_int*4-15)
			else
				c.set_Posicion_y(60)
				c.set_Posicion_x(500)
		//print iman.Valor_int.to_string()
		
	def on_numero(c:Control)
		sonidos.play("archivo",dd+"soinuak/zenbakiak/"+tostring(3,c.Valor_int)+".ogg")
		sdlk.Focus=-1
		
		
	def on_tomar_hamarreko (c:Control)
		if sdlk.colision_mascara(iman,c)
			iman.Valor_int-=c.Valor_int
			c.Valor_bool=false
		var pos=0
		for var x=0 to ultimo_de_lista(hamarreko)
			if hamarreko[x].Valor_bool
				hamarreko[x].set_Posicion_y((iman.dr.y+iman.dr.h/2)-(hamarreko[x].dr.h/2))
				hamarreko[x].set_Posicion_x(iman.dr.x+pos*40+25)
				pos++
		//print iman.Valor_int.to_string()
	
	
	def on_salir (c:Control)
		sonidos.play("clik")
		fin=true
		comando=-1
	
		
	def on_zuzendu(c:Control)
		if iman.Valor_int== solucion
			sonidos.play("ondo")
			fin=true
			comando=14
		else
			sonidos.play("gaizki")
			
		
	def on_continuar (c:Control)
		sonidos.play("clik")
		fin=true
		comando=14
		
				

