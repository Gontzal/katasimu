uses Gee
uses SDL
uses SDLImage
uses SDLGraphics
uses SDLTTF
uses GLib

class Sdlk:Object
	Ancho:int16
	Alto:int16
	lista_general: list of Control
	lista_profundidad: list of int
	pantalla:unowned SDL.Screen
	Referencia_general:int=0
	Focus:int=-1
	Puntero_x:int
	Puntero_y:int
	Rec:SDL.Rect
	musica2:SDLMixer.Music
	Teclas: dict of string,bool
	
	event tecleado(s:string)
	event tecleado_up(s:string)
	event update()
	
	init
		Teclas= new dict of string,bool
		
		lista_profundidad= new list of int
		lista_general=new list of Control
		
	construct (ancho:int16,alto:int16)
		Ancho=ancho
		Alto=alto
	
	def get_transparente (x:int,y:int,img:SDL.Surface): uchar
		dire:uint8*
		alpha:uchar=0
		
		img.do_lock()
		dire=img.pixels 
		dire+= (y*img.pitch)+ (x*img.format.BytesPerPixel)
		
		case img.format.BytesPerPixel
			when 1,2,3
				alpha=255
				
			when 4
				dire+=+3
				alpha=*dire
		img.unlock()
		return alpha

	def get_R (x:int,y:int,img:SDL.Surface): uchar
		dire:uint8*
		color:uchar=0
		
		img.do_lock()
		dire=img.pixels
		dire+= (y*img.pitch)+ x*(img.format.BytesPerPixel)
		color=*dire
		img.unlock()
		return color
		
	def get_G (x:int,y:int,img:SDL.Surface): uchar
		dire:uint8*
		color:uchar=0
		img.do_lock()
		dire=img.pixels 
		dire+= (y*img.pitch)+ (x*img.format.BytesPerPixel)
		dire+=1
		color=*dire
		img.unlock()
		return color
	
	def get_B (x:int,y:int,img:SDL.Surface): uchar
		dire:uint8*
		color:uchar=0
		img.do_lock()
		dire=img.pixels 
		dire+= (y*img.pitch)+ (x*img.format.BytesPerPixel)
		dire+=2
		color=*dire
		img.unlock()
		return color
		
	def set_R (x:int,y:int,ref img:SDL.Surface,color:uchar)
		dire:uint8*
		img.do_lock()
		dire=img.pixels 
		dire+= (y*img.pitch)+ (x*img.format.BytesPerPixel)
		*dire=color
		img.unlock()
		
	def set_G (x:int,y:int,ref img:SDL.Surface,color:uchar)
		dire:uint8*
		img.do_lock()
		dire=img.pixels 
		dire+= (y*img.pitch)+ (x*img.format.BytesPerPixel)
		dire+=1
		*dire=color
		img.unlock()
		
		
	def set_B (x:int,y:int,ref img:SDL.Surface,color:uchar)
		dire:uint8*
		img.do_lock()
		dire=img.pixels 
		dire+= (y*img.pitch)+ (x*img.format.BytesPerPixel)
		dire+=2
		*dire=color
		img.unlock()
		
		
	def set_transparente (x:int,y:int,ref img:SDL.Surface,color:uchar)
		dire:uint8*
		img.do_lock()
		dire=img.pixels 
		dire+= (y*img.pitch)+ (x*img.format.BytesPerPixel)
		dire+=+3
		*dire=color
		img.unlock()
		
	def abrillanta_imagen(ref img:SDL.Surface)
		
		var dif = 60 // el grado de CLARIDAD
		for var i=0 to ((img.h-1))
			for var j=0 to ((img.w-1))
				var R=(int)get_R(j,i,img)
				var G=(int)get_G(j,i,img)
				var B=(int)get_B(j,i,img)
				if R+dif <= 255
					set_R(j,i,ref img,(uchar)R+dif )
				else
					set_R(j,i,ref img,255 )

				if G+dif <= 255
					set_G(j,i,ref img,(uchar)G+dif )
				else
					set_G(j,i,ref img,255 )
					
				if B+dif <= 255
					set_B(j,i,ref img,(uchar)B+dif )
				else
					set_B(j,i,ref img,255 )
				
	def genera_elemento(este:Control)
		var lugar=-1
		for var i=0 to ultimo_de_lista(lista_general)
			if not lista_profundidad.contains(i)
				lugar=i
				break
		
		if lugar==-1
			este.Index=sdlk.Referencia_general
			sdlk.lista_general.add(este)
			sdlk.lista_profundidad.add(este.Index)
			sdlk.Referencia_general++
		else
			este.Index=lugar
			sdlk.lista_profundidad.add(lugar)
			sdlk.lista_general.remove_at(lugar)
			sdlk.lista_general.insert(lugar,este)
			



	//revisa las colisiones entre dos imagenes 
	def colision_cuadrada(a:Control,b:Control):bool
		var res=false
		if (a.Index!=b.Index) and (a.Visible) and (b.Visible)
			/*a - bottom right co-ordinates*/
			var ax=a.dr.x
			var ay=a.dr.y
			var ax1 = a.dr.x + a.dr.w - 1;
			var ay1 = a.dr.y + a.dr.h - 1;
			
			/*b - bottom right co-ordinates*/
			var bx=b.dr.x
			var by=b.dr.y
			var bx1 = b.dr.x + b.dr.w - 1;
			var by1 = b.dr.y + b.dr.h - 1;

			/*check if bounding boxes intersect*/
			var rectcolision=true
			if((bx1 < ax) or (ax1 < bx)) do rectcolision=false
			if((by1 < ay) or (ay1 < by)) do rectcolision=false
			res=rectcolision
			
		return res
		
		
	
	def colision_mascara(a:Control,b:Control):bool
		//no funciona si las imagenes han sido ampliadas o reducidas.
		var res=false
		if (a.Index!=b.Index) and (a.Visible) and (b.Visible)
			/*a - bottom right co-ordinates*/
			var ax=a.dr.x
			var ay=a.dr.y
			var ax1 = a.dr.x + a.dr.w - 1;
			var ay1 = a.dr.y + a.dr.h - 1;
			
			/*b - bottom right co-ordinates*/
			var bx=b.dr.x
			var by=b.dr.y
			var bx1 = b.dr.x + b.dr.w - 1;
			var by1 = b.dr.y + b.dr.h - 1;

			/*check if bounding boxes intersect*/
			var rectcolision=true
			if((bx1 < ax) or (ax1 < bx)) do rectcolision=false
			if((by1 < ay) or (ay1 < by)) do rectcolision=false
			
			if rectcolision
				if (a.Nombre=="Imagen") and (b.Nombre=="Imagen")
					//recoge el cuadrado de interseccion
					var x0 = int.max(ax,bx)
					var y0 = int.max(ay,by)
					var x1 = int.min(ax1,bx1)
					var y1 = int.min(ay1,by1)
					
					var aw=a.dr.w 
					var ah=a.dr.h
					var bw=b.dr.w 
					var bh=b.dr.h 
					res=false
					
					
					//recorre el cuadrado de intersección usando la mascara la mascara tiene la coordenada X reducida en 8.
					for var j=y0 to (y1-1)
						for var i=x0 to (x1-1)
							if ( a.Mascara[(i-ax)  +(j-ay)* aw  ] != ' ' )  and  ( b.Mascara[ (i-bx)+(j-by)*bw ] != ' ' )
								res=true
								break
				else
					if (a.Nombre=="Imagen") 
						//recoge el cuadrado de interseccion
						var x0 = int.max(ax,bx)
						var y0 = int.max(ay,by)
						var x1 = int.min(ax1,bx1)
						var y1 = int.min(ay1,by1)
						
						var aw=a.dr.w 
						var ah=a.dr.h
						var bw=b.dr.w 
						var bh=b.dr.h 
						res=false
						//recorre el cuadrado de intersección usando la mascara la mascara tiene la coordenada X reducida en 8.
						for var j=y0 to (y1-1)
							for var i=x0 to (x1-1)
								////print"imprime -%c-",a.Mascara[(i-ax)  +(j-ay)* aw  ]
								if ( a.Mascara[(i-ax)  +(j-ay)* aw  ] != ' ' ) 
									res=true
									break
					else	
						if (b.Nombre=="Imagen") 
							//recoge el cuadrado de interseccion
							var x0 = int.max(ax,bx)
							var y0 = int.max(ay,by)
							var x1 = int.min(ax1,bx1)
							var y1 = int.min(ay1,by1)
							
							var aw=a.dr.w 
							var ah=a.dr.h
							var bw=b.dr.w 
							var bh=b.dr.h 
							res=false
							//recorre el cuadrado de intersección usando la mascara la mascara tiene la coordenada X reducida en 8.
							for var j=y0 to (y1-1)
								for var i=x0 to (x1-1)
									////print"imprime -%c-",a.Mascara[(i-ax)  +(j-ay)* aw  ]
									if ( b.Mascara[(i-bx)  +(j-by)* bw  ] != ' ' ) 
										res=true
										break
						else
							res=true
					
				
		return res

	def convertir_en_mascara (img:SDL.Surface):list of uchar
		str:list of uchar
		str = new list of uchar
		str.clear()
		lista: list of uchar
		lista=new list of uchar
		////print"12345678901234567890"
		var c=0
		for var j=0 to (img.h-1)
			for var i=0 to (img.w-1)
				if sdlk.get_transparente(i,j,img)>50
					str.add('x')
				else
					str.add(' ')				
		return str

	def pintar()
		var c=0
		while c<=ultimo_de_lista(lista_profundidad)
			if lista_profundidad[c]!=sdlk.Focus do this.lista_general[ lista_profundidad[c] ].pinta()
			c++
		if (sdlk.Focus>-1) do this.lista_general[ sdlk.Focus ].pinta()
			
		screen.flip()
		
	def Rectangulo_redondo(screen:Screen,dr:Rect,r:int16,R:uchar,G:uchar,B:uchar,A:uchar)
		var x= (int16)dr.x
		var y= (int16)dr.y
		var w= (int16)dr.w
		var h= (int16)dr.h
		
		Arc.fill_rgba(screen,x+r,y+r,r,180,270,R,G,B,A)
		Arc.fill_rgba(screen,x-r+w,y+r,r,270,360,R,G,B,A)
		Arc.fill_rgba(screen,x+r,y-r+h,r,90,180,R,G,B,A)
		Arc.fill_rgba(screen,x+w-r,y+h-r,r,0,90,R,G,B,A)
		Rectangle.fill_rgba(screen,x+r+1, y, w+x-r-1,y+r, R,G,B,A )
		Rectangle.fill_rgba(screen,x+r+1 ,y+h-r, w+x-r-1,y+h, R,G,B,A )
		Rectangle.fill_rgba(screen,x, y+r+1,w+x, h+y-r-1, R,G,B,A )
		
	def Rectangulo_redondo_borde(screen:Screen,dr:Rect,r:int16,R:uchar,G:uchar,B:uchar,A:uchar,BR:uchar,BG:uchar,BB:uchar,BA:uchar)
		var x= (int16)dr.x
		var y= (int16)dr.y
		var w= (int16)dr.w
		var h= (int16)dr.h
		
		Arc.fill_rgba(screen,x+r,y+r,r,180,270,R,G,B,A)
		Arc.fill_rgba(screen,x-r+w,y+r,r,270,360,R,G,B,A)
		Arc.fill_rgba(screen,x+r,y-r+h,r,90,180,R,G,B,A)
		Arc.fill_rgba(screen,x+w-r,y+h-r,r,0,90,R,G,B,A)
		Rectangle.fill_rgba(screen,x+r+1, y, w+x-r-1,y+r, R,G,B,A )
		Rectangle.fill_rgba(screen,x+r+1 ,y+h-r, w+x-r-1,y+h, R,G,B,A )
		Rectangle.fill_rgba(screen,x, y+r+1,w+x, h+y-r-1, R,G,B,A )
		
		a:int16[3] ; b:int16[3]
		
		a={x+r, x+2, x } ; 	b= {y,   y+2, y+r }
		BezierCurve.rgba (screen, a, b, 3, 3, BR,BG,BB,BA)
		
		a={x+r, x+2, x } ; 	b= {y+h,   y+ h-2, y+h-r }
		BezierCurve.rgba (screen, a, b, 3, 3, BR,BG,BB,BA)
		
		a={x+w-r, x+w-2, x+w } ; 	b= {y,   y+2, y+r }
		BezierCurve.rgba (screen, a, b, 3, 3, BR,BG,BB,BA)
		
		a={x+w-r, x+w, x+w } ; 	b= {y+h, y+ h, y+h-r }
		BezierCurve.rgba (screen, a, b, 3, 3,BR,BG,BB,BA)
		
		Line.rgba (screen,x+r,y,x+w-r,y,BR,BG,BB,BA)
		Line.rgba (screen,x+r,y+h,x+w-r,y+h,BR,BG,BB,BA)
		
		Line.rgba (screen,x,y+r,x,y+h-r,BR,BG,BB,BA)
		Line.rgba (screen,x+w,y+r,x+w,y+h-r,BR,BG,BB,BA)
		
		
		
		
	def Boton_redondo(screen:Screen,dr:Rect,r:int16,R:uchar,G:uchar,B:uchar,A:uchar)
		var x= (int16)dr.x
		var y= (int16)dr.y
		var w= (int16)dr.w
		var h= (int16)dr.h
		var R2=R+5
		var G2=G+5
		var B2=B+5
		
		Arc.fill_rgba(screen,x+r,y+r,r,180,270,R2,G2,B2,A)
		Arc.fill_rgba(screen,x-r+w,y+r,r,270,360,R2,G2,B2,A)
		Arc.fill_rgba(screen,x+r,y-r+h,r,90,180,R,G,B,A)
		Arc.fill_rgba(screen,x+w-r,y+h-r,r,0,90,R,G,B,A)
		
		
		Rectangle.fill_rgba(screen,x+r+1, y, w+x-r-1,y+r, R2,G2,B2,A )
		Rectangle.fill_rgba(screen,x+r+1 ,y+h-r, w+x-r-1,y+h, R,G,B,A )
		Rectangle.fill_rgba(screen,x, y+r+1, w+x, (h/2)+y-1, R2,G2,B2,A )
		Rectangle.fill_rgba(screen,x, y+(h/2),w+x, h+y-r-1, R,G,B,A )
		
		a:int16[3] ; b:int16[3]
		
		a={x+r, x+2, x } ; 	b= {y,   y+2, y+r }
		BezierCurve.rgba (screen, a, b, 3, 3, 255,255,255,255)
		
		a={x+r, x+2, x } ; 	b= {y+h,   y+ h-2, y+h-r }
		BezierCurve.rgba (screen, a, b, 3, 3, 255,255,255,255)
		
		a={x+w-r, x+w-2, x+w } ; 	b= {y,   y+2, y+r }
		BezierCurve.rgba (screen, a, b, 3, 3, 255,255,255,255)
		
		a={x+w-r, x+w, x+w } ; 	b= {y+h, y+ h, y+h-r }
		BezierCurve.rgba (screen, a, b, 3, 3, 255,255,255,255)
		
		Line.rgba (screen,x+r,y,x+w-r,y,255,255,255,255)
		Line.rgba (screen,x+r,y+h,x+w-r,y+h,255,255,255,255)
		
		Line.rgba (screen,x,y+r,x,y+h-r,255,255,255,255)
		Line.rgba (screen,x+w,y+r,x+w,y+h-r,255,255,255,255)
		
		
		

	def colision_n (a:int,b:int):bool
		var oa= lista_general[a].dr
		var ob= lista_general[b].dr
		var exit=false
		if (lista_general[a].Visible) and (lista_general[b].Visible) and (a!=b)
			var ax=oa.x
			var ay=oa.y
			var ax1 = oa.x + oa.w - 1;
			var ay1 = oa.y + oa.h - 1;
			
			var bx=ob.x
			var by=ob.y
			var bx1 = ob.x + ob.w - 1;
			var by1 = ob.y + ob.h - 1;

			/*check interseccion*/
			exit=true
			if((bx1 < ax) or (ax1 < bx)) do exit=false
			if((by1 < ay) or (ay1 < by)) do exit=false
		
		return exit
		
	def colision_xy (x:int,y:int,c:Control):bool
		var r=true
		if (x<c.dr.x ) or (x > c.dr.x+c.dr.w ) do r=false
		if (y<c.dr.y ) or (y > c.dr.y+c.dr.h ) do r=false
		return r
		
	def muere_todo()
		var contr= ultimo_de_lista(lista_profundidad)
		while contr>=0
			var c=lista_general[lista_profundidad[contr]]
			c.muere()
			contr--
		
	
	def update_control()
		sdlk.update()
		var contr= ultimo_de_lista(lista_profundidad)
		while contr>=0
			var c=lista_general[lista_profundidad[contr]]
			c.update(c)
			contr--
	
	def mira_sobre()// mira que se ponga sobre las cosas
		if ultimo_de_lista(lista_profundidad)>=0
			var ctr= ultimo_de_lista(lista_profundidad)
			while ctr>=0
				var c=lista_general[lista_profundidad[ctr]]
				if (sdlk.colision_xy(Puntero_x,Puntero_y,c)) and (c.Visible)
					c.sobre(c)
					break
				ctr--
			
	def pos_x(posicionx:int):int // introduce una posicion relativa entre 0,100 del ancho de la pantalla
		return sdlk.Ancho*posicionx/100

	def pos_y(posiciony:int):int // introduce una posicion relativa entre 0,100 del altura de la pantalla
		return sdlk.Alto*posiciony/100
		
	def anchura(w:int):int // anchura relativa del objeto
		return sdlk.Ancho*w/100
	
	def altura(h:int):int // altura relativa del objeto
		return sdlk.Alto*h/100
		
	def tamano (escala:int):int
		var media= (sdlk.Alto+sdlk.Ancho)/2
		return media*escala/100
		
	def toma_eventos(eventos:Event)
		if ultimo_de_lista(lista_profundidad)>=0
			case eventos.type
				when EventType.MOUSEBUTTONUP
					Puntero_x=eventos.button.x
					Puntero_y=eventos.button.y
					if sdlk.Focus!=-1
						var c=lista_general[sdlk.Focus]
						if  (eventos.button.button==1) //and (sdlk.colision_xy(eventos.button.x,eventos.button.y,c))
							c.soltado (c)
							
						//c.soltar_botones(c.Index)
									
				when EventType.MOUSEBUTTONDOWN
					Puntero_x=eventos.button.x
					Puntero_y=eventos.button.y
					sdlk.Focus=-1
					var contr= ultimo_de_lista(lista_profundidad)
					case  (eventos.button.button)
						when 1
							while contr>=0
								var c=lista_general[lista_profundidad[contr]]
								if (c.Visible) and (sdlk.colision_xy(eventos.button.x,eventos.button.y,c))
									var x =(uint8)(sdlk.Puntero_x- c.dr.x) //indica el lugar xy desde donde se engancha el boton
									var y = (uint8)(sdlk.Puntero_y - c.dr.y) // para poder ser arrastrado manteniendo este punto.
									if (c.Nombre=="Imagen") 
										if (c.get_alpha(x, y)>5) 
											c.izq_pulsado(c)
											break
									else
										c.izq_pulsado(c)
										break
								contr--

				when EventType.MOUSEMOTION
					Puntero_x=eventos.button.x
					Puntero_y=eventos.button.y
					
					var contr= ultimo_de_lista(lista_profundidad)
					case  (eventos.button.button)
						when 1
							while contr>=0
								var c=lista_general[lista_profundidad[contr]]
								if (sdlk.colision_xy(eventos.button.x,eventos.button.y,c))
									c.izq_sobre(c)
									break
								contr--
				
							
						
				when EventType.KEYUP
					Teclas[SDL.Key.get_name(eventos.key.keysym.sym)]=false
					sdlk.tecleado_up(SDL.Key.get_name(eventos.key.keysym.sym))
				
				when EventType.KEYDOWN
					if sdlk.Focus>-1 do 
						var c=lista_general[sdlk.Focus]
						if c.Nombre=="Entrada"
							var sb = new StringBuilder ();
							sb.append_unichar (eventos.key.keysym.unicode);
							
							if valida_sp(sb.str)
								c.tecleado(c.Index,sb.str)
							else
								c.tecleado(c.Index,"Teclado:"+SDL.Key.get_name(eventos.key.keysym.sym))
								if SDL.Key.get_name(eventos.key.keysym.sym)=="return"
									c.enter(c)
					
					Teclas[SDL.Key.get_name(eventos.key.keysym.sym)]=true
					sdlk.tecleado(SDL.Key.get_name(eventos.key.keysym.sym))
				
				when EventType.VIDEORESIZE
					print "%d x %d",eventos.resize.w,eventos.resize.h
					sdlk.Ancho=(int16)eventos.resize.w
					sdlk.Alto=(int16)eventos.resize.h
					screen = SDL.Screen.set_video_mode (sdlk.Ancho,sdlk.Alto,25, SurfaceFlag.DOUBLEBUF | SurfaceFlag.HWACCEL | SurfaceFlag.HWSURFACE | SurfaceFlag.RESIZABLE)
					for var i=0 to ultimo_de_lista(lista_general)
						if lista_general[i].Nombre=="Grilla"
							lista_general[i].refresca()
							print "refrescando"

class caja:Object
	ocupada:bool=false
	centrada:bool=true
	w:int
	h:int
	x:int
	y:int
	obj: unowned Control
	obj_h: int
	obj_w: int
	
class fila:Object
	w:int
	h:int
	x:int
	y:int
	cajas: list of caja
	init
		cajas= new list of caja

class grilla:Control
	grid: list of fila
	mifila:fila
	total_filas: int
	init
		print "iniciando la grilla"
		grid= new list of fila
		this.Nombre="Grilla"
	construct (filas:int)
		total_filas=filas
		var x=5
		var y=5
		var h= sdlk.Alto/filas
		var w= sdlk.Ancho
		for var i=0 to (filas-1)
			mifila= new fila
			mifila.x = x
			mifila.y = y
			mifila.h = h
			mifila.w = w
			grid.add(mifila)
			
			print "fila %d, con x:%d,  y:%d,  w:%d,  h:%d",i,x,y,h,w
			y+=h
		print "filas creadas %d",filas
		sdlk.genera_elemento(this)
		
	def override refresca ()
		print "comienza refresco"
		
		if (tamano_de_lista(grid)>0)
			var x=5
			var y=5
			var h= (sdlk.Alto)/tamano_de_lista(grid) 
			var w= (sdlk.Ancho)
			for var i=0 to ultimo_de_lista(grid)
				grid[i].x=x
				grid[i].y=y
				grid[i].h=h
				grid[i].w=w
				y+=h
				
		for var f=0 to ultimo_de_lista(grid)
			if tamano_de_lista(grid[f].cajas)>0	
				var x1 = grid[f].x
				var w1 = sdlk.Ancho /(tamano_de_lista(grid[f].cajas))
				var h1 = grid[f].h
				var y1 = grid[f].y 
				 
				for var i=0 to ultimo_de_lista(grid[f].cajas)
					grid[f].cajas[i].x=x1
					grid[f].cajas[i].h=h1
					grid[f].cajas[i].y=y1
					grid[f].cajas[i].w=w1
					
					print "refrescando de fila %d, x:%d,  y:%d,  w:%d,  h:%d",f,x1,y1,w1,h1
					x1+= w1
		
		
		for var i=0 to ultimo_de_lista(grid)
			for var j=0 to ultimo_de_lista(grid[i].cajas)
				var cajita=grid[i].cajas[j]
				case cajita.obj.Nombre
					when"CuadradoRedondo"
						cajita.obj.set_Posicion_x ( cajita.x  )
						cajita.obj.set_Posicion_y ( cajita.y  )
						cajita.obj.dr.w =(int16) cajita.w 
						cajita.obj.dr.h =(int16) cajita.h 
					when "Imagen"
						var tam_cajita= int.min (cajita.h,cajita.w)
						var tam_original= int.min (cajita.obj_h, cajita.obj_w)
						
						if tam_cajita<tam_original
							cajita.obj.set_Tamano(tam_cajita,tam_cajita)
						cajita.obj.set_Posicion_x ( cajita.x + cajita.w/2 - cajita.obj.get_Ancho()/2)
						cajita.obj.set_Posicion_y ( cajita.y + cajita.h/2 - cajita.obj.get_Alto()/2)
						
					when "Etiqueta"
						if (not cajita.obj.Arrastrado and cajita.obj.Arrastrable) or (not cajita.obj.Arrastrable)
							cajita.obj.set_Posicion_x ( cajita.x + cajita.w/2 - cajita.obj.get_Ancho()/2)
							cajita.obj.set_Posicion_y ( cajita.y + cajita.h/2 - cajita.obj.get_Alto()/2)
						else
							if cajita.obj.get_Posicion_x()>sdlk.Ancho do cajita.obj.set_Posicion_x(sdlk.Ancho-cajita.obj.get_Ancho()/2)
							if cajita.obj.get_Posicion_y()>sdlk.Alto do cajita.obj.set_Posicion_y(sdlk.Alto-cajita.obj.get_Alto()/2)
		print "fin"
		
		
		
		
		
		
		
	def add (dato_fila:int,objeto:Control)
		if (dato_fila>total_filas)
			print "no se puede insertar fila en :%d",dato_fila
		else
			micaja : caja=new caja
			grid[dato_fila].cajas.add(micaja)
			print "existen: en fila %d, %d cajas",dato_fila,tamano_de_lista(grid[dato_fila].cajas)
			
			if tamano_de_lista(grid[dato_fila].cajas)==1
				grid[dato_fila].cajas[0].x= grid[dato_fila].x
				grid[dato_fila].cajas[0].y= grid[dato_fila].y
				grid[dato_fila].cajas[0].w= grid[dato_fila].w
				grid[dato_fila].cajas[0].w= grid[dato_fila].h 
				
				grid[dato_fila].cajas[0].obj=objeto
				grid[dato_fila].cajas.last().obj_w = grid[dato_fila].cajas.last().obj.get_Ancho()
				grid[dato_fila].cajas.last().obj_h = grid[dato_fila].cajas.last().obj.get_Alto()
				
			else
				var x1= grid[dato_fila].x
				var w1= grid[dato_fila].w /(tamano_de_lista(grid[dato_fila].cajas))
				var h1= grid[dato_fila].h 
				var y1= grid[dato_fila].y 
				 
				for var i=0 to ultimo_de_lista(grid[dato_fila].cajas)
					grid[dato_fila].cajas[i].x=x1
					grid[dato_fila].cajas[i].h=h1
					grid[dato_fila].cajas[i].y=y1
					grid[dato_fila].cajas[i].w=w1
					
					print "datos de fila %d, x:%d,  y:%d,  w:%d,  h:%d",dato_fila,x1,y1,w1,h1
					x1+= w1
					
				grid[dato_fila].cajas.last().obj=objeto
				
				grid[dato_fila].cajas.last().obj_w = grid[dato_fila].cajas.last().obj.get_Ancho()
				grid[dato_fila].cajas.last().obj_h = grid[dato_fila].cajas.last().obj.get_Alto()
					
	// centrar todos los objetos en las celdas y
	// aumentar el tamaño de los objetos para que ocupen las celdas en su totalidad.	
		
			
class Control:Object
	Ref: int
	Index: int
	Nombre: string
	Imagen_0 : SDL.Surface
	Imagen_1 : SDL.Surface
	Imgfondo : SDL.Surface
	Imagen_sobre : SDL.Surface
	Imagen_extra:SDL.Surface
	
	Texto: string
	Valor_str: string
	Valor_str2: string
	Valor_int: int
	Valor_int2: int
	Valor_bool:bool
	
	Resaltar_Sobre:bool=true
	Visible:bool=true
	Fondo_Visible:bool=true
	Arrastrable:bool=true
	Arrastrado:bool=false
	Tomado:bool=false
	Tomado_x:int=0
	Tomado_y:int=0
	TamanoTexto:int=12
	TextoVisible:bool=true
	ColorTexto:SDL.Color
	ColorFondo:SDL.Color
	ColorControl:SDL.Color
	ColorBorde:SDL.Color
	Borde:bool=true
	Valor_num: int
	Posicion_x: int
	Posicion_y: int
	Tamano_x: int
	Tamano_y: int
	sr:SDL.Rect
	dr:SDL.Rect
	srt:SDL.Rect
	drt:SDL.Rect
	Pulsado:int=0
	Mascara:list of uchar
	FiguraFondo:bool=false
	
	event izq_pulsado(c:Control)
	event der_pulsado(c:Control)
	event soltado(c:Control)
	event sobre(c:Control)
	event izq_sobre(c:Control)
	event tecleado(a:int,t:string)
	event enter(c:Control)
	event update(c:Control)
	
	init 
		this.Valor_str=""
		this.Texto=""
		
	def virtual pinta()
		pass
	
	def virtual set_Posicion_x(a:int)
		pass
	
	def virtual get_Ancho():int
		return 0
	def virtual refresca()
		pass
	def virtual get_Alto():int
		return 0
		
	def virtual set_ColorBorde(r:uchar,g:uchar,b:uchar,a:uchar)
		pass
		
	def virtual set_ColorControl(r:uchar,g:uchar,b:uchar,a:uchar)
		pass
	
	def virtual set_Posicion_y(a:int)
		pass
	
	def virtual set_Alto(a:int)
		pass
	
	def virtual set_Ancho(a:int)
		pass
	
	def virtual get_Posicion_x():int
		return 0

	def virtual set_Texto(a:string)
		pass
	
	def virtual get_Texto():string
		return ""
	
	def virtual get_Posicion_y():int
		return 0
	def virtual set_Tamano(w:int,h:int)
		pass
		
	def virtual set_Posicion(a:int,b:int)
		pass
	def virtual get_alpha (x:int,y:int): uchar
		return 0
	def soltar_botones (a:int)
		var c=sdlk.lista_general[a]
		if c.Pulsado==1 do c.Pulsado=2
	def muere()
		if sdlk.lista_profundidad.contains(this.Index)
			var x=sdlk.lista_profundidad.index_of(this.Index)
			sdlk.lista_profundidad.remove_at(x)
			for var i=0 to ultimo_de_lista(sdlk.lista_general)
				if sdlk.lista_general[i].Index == this.Index
					var vacio=new Control
					sdlk.lista_general.remove_at(i)
					sdlk.lista_general.insert(i,vacio)
					
					
// La clase que define los botones de imagen					
class Imagen: Control
	color:SDL.Color
	Fuente:SDLTTF.Font
	pasando:bool=false
	
	construct (referencia:int,posicion_x:int,posicion_y:int, nombreimagen:string)
		
		this.Ref=referencia
		this.Posicion_x=posicion_x
		this.Posicion_y=posicion_y
		this.Nombre="Imagen"
		this.Texto=" "
		this.Imagen_0= SDLImage.load (nombreimagen)
		this.Imagen_sobre= SDLImage.load (nombreimagen)
		this.Imagen_extra= SDLImage.load (nombreimagen)
		sdlk.abrillanta_imagen(ref this.Imagen_sobre)
		
		this.sr.x = 0;this.sr.y = 0; this.sr.w = (uint16)this.Imagen_0.w; this.sr.h= (uint16)this.Imagen_0.h
		this.dr.x = (int16)posicion_x;  this.dr.y =(int16)posicion_y;  this.dr.w =this.sr.w;   this.dr.h = this.sr.h;
		this.Mascara=sdlk.convertir_en_mascara(this.Imagen_0)
		Fuente= new Font(dd+"datuak/KatamotzIkasi.ttf",this.TamanoTexto)
		this.ColorTexto.r=25
		this.ColorTexto.g=25
		this.ColorTexto.b=25
		this.ColorTexto.unused=25
		
		this.Imagen_1= Fuente.render_blended_utf8(this.Texto,this.ColorTexto)
		
		this.srt.x = 0; this.srt.y = 0; 
		this.srt.w = (uint16)this.Imagen_1.w;
		this.srt.h= (uint16)this.Imagen_1.h
		
		this.drt.x = (int16)posicion_x +(this.sr.w/2)-(this.srt.w/2);         
		this.drt.y =(int16)posicion_y+(this.sr.h/2)-(this.srt.w/2);  
		this.drt.w =this.srt.w;   this.drt.h = this.srt.h;
		
		sdlk.genera_elemento(this)
		
		this.izq_pulsado.connect(control_pulsado)
		this.soltado.connect(control_soltado)
		this.sobre.connect(control_sobre)
		
	def set_Imagen(nombreimagen:string)
		this.Imagen_0 = load (nombreimagen)
		this.Imagen_extra = load (nombreimagen)
		this.Imagen_sobre= SDLImage.load (nombreimagen)
		sdlk.abrillanta_imagen(ref this.Imagen_sobre)
		
		this.sr.w = (uint16)this.Imagen_0.w;
		this.sr.h = (uint16)this.Imagen_0.h;
		this.dr.w = (uint16)this.Imagen_0.w;
		this.dr.h = (uint16)this.Imagen_0.h;
		
	def set_Texto(nuevotexto:string)
		
		Fuente= new Font("KatamotzIkasi.ttf",17)
		color.r=221;color.g=251;color.b=240
		this.Imagen_1 = Fuente.render_blended_utf8(nuevotexto,color)
		this.srt.w = (uint16)this.Imagen_1.w;
		this.srt.h = (uint16)this.Imagen_1.h;
		this.drt.w = (uint16)this.Imagen_1.w;
		this.drt.h = (uint16)this.Imagen_1.h;
		this.Texto = nuevotexto;

	def override set_Posicion(x:int,y:int)
		this.dr.x=(int16)x
		this.dr.y=(int16)y
		this.drt.x = (int16)this.dr.x +(this.sr.w/2)-(this.srt.w/2);         
		this.drt.y =(int16)this.dr.y+(this.sr.h/2)-(this.srt.w/2)
		
	def override get_Ancho ():int
		return (int)this.dr.w
		
	def override get_Alto ():int
		return (int)this.dr.h
		
	def override set_Posicion_y(y:int)
		this.dr.y=(int16)y
		this.drt.y =(int16)this.dr.y+(this.sr.h/2)-(this.srt.w/2)

	def override set_Posicion_x(x:int)
		this.dr.x=(int16)x
		this.drt.x = (int16)this.dr.x +(this.sr.w/2)-(this.srt.w/2);         
	
	def override get_Posicion_x(): int 
		return (int)this.dr.x
	
	def override get_Posicion_y(): int 
		return (int)this.dr.y
	
		
	def override set_Tamano(w:int,h:int)
		wa:double
		ha:double
		wa=(double)w/(double)this.Imagen_extra.w
		ha=(double)h/(double)this.Imagen_extra.h

		this.Imagen_0= RotoZoom.zoom(this.Imagen_extra,wa,ha,1)
		this.Imagen_sobre= RotoZoom.zoom(this.Imagen_extra,wa,ha,1)
		this.sr.w = (uint16)this.Imagen_0.w;
		this.sr.h = (uint16)this.Imagen_0.h;
		this.dr.w = (uint16)this.Imagen_0.w;
		this.dr.h = (uint16)this.Imagen_0.h;
		this.drt.x = (int16)this.dr.x +(this.sr.w/2)-(this.srt.w/2);         
		this.drt.y =(int16)this.dr.y+(this.sr.h/2)-(this.srt.w/2);
		this.Mascara=sdlk.convertir_en_mascara(this.Imagen_0)
		
	def control_sobre(c:Control)
		
		this.Tomado_x = sdlk.Puntero_x- this.dr.x //indica el lugar xy desde donde se engancha el boton
		this.Tomado_y = sdlk.Puntero_y - this.dr.y // para poder ser arrastrado manteniendo este punto.
		if (c.Resaltar_Sobre) and (get_alpha((int)this.Tomado_x, (int)this.Tomado_y)>5) and (c.Visible)
			pasando=true
		
	// evento soltado
	def control_soltado(c:Control)
		if c.Arrastrable
			c.Tomado=false
		
	// evento pulsado
	def control_pulsado(c:Control)
		
		this.Tomado_x = sdlk.Puntero_x- this.dr.x //indica el lugar xy desde donde se engancha el boton
		this.Tomado_y = sdlk.Puntero_y - this.dr.y // para poder ser arrastrado manteniendo este punto.
		// "%d",get_alpha((int8)this.Tomado_x, (int8)this.Tomado_y)
		if (c.Arrastrable) and (get_alpha((int)this.Tomado_x, (int)this.Tomado_y)>5) and (c.Visible)
			c.Tomado=true
			c.Arrastrado=true
			if not c.FiguraFondo
				sdlk.Focus=c.Index
			else
				sdlk.Focus=-1
		
			
		
	def override get_alpha (x:int,y:int): uchar
		dire:uint8*
		alpha:uchar=0
		
		if (x<this.Imagen_0.w) and (y<this.Imagen_0.h)
			this.Imagen_0.do_lock()
			dire=this.Imagen_0.pixels 
			dire+= (y*this.Imagen_0.pitch)+ (x*this.Imagen_0.format.BytesPerPixel)
			case this.Imagen_0.format.BytesPerPixel
				when 1,2,3
					alpha=255
				when 4
					dire+=+3
					alpha=*dire
			this.Imagen_0.unlock()
		return alpha

	def override pinta ()
		if this.Visible
			if (this.Tomado) 
				this.dr.x=(int16)sdlk.Puntero_x-(int16)this.Tomado_x
				if (this.dr.x+this.dr.w <0) do this.dr.x=5
				if (this.dr.x > sdlk.Ancho) do this.dr.x=sdlk.Ancho-(int16)this.dr.w-5
				
				this.dr.y=(int16)sdlk.Puntero_y-(int16)this.Tomado_y
				if (this.dr.y+this.dr.h <0) do this.dr.y=5
				if (this.dr.y >sdlk.Alto) do this.dr.y=sdlk.Alto-(int16)this.dr.h-5

				this.drt.x = (int16)this.dr.x +(this.sr.w/2)-(this.srt.w/2);         
				this.drt.y =(int16)this.dr.y+(this.sr.h/2)-(this.srt.w/2);
			if this.pasando	
				this.Imagen_sobre.blit(this.sr, screen, this.dr)
			else
				this.Imagen_0.blit(this.sr, screen, this.dr)
			pasando=false
				
			if this.TextoVisible
				this.Imagen_1.blit(this.srt, screen, this.drt)
				

	
		
class Obj_lineas_texto: Object
	linea_texto: string
	linea_imagen:  SDL.Surface
	linea_srt:  SDL.Rect
	linea_drt: SDL.Rect
	

class Etiqueta: Control
	color:SDL.Color
	Fuente:SDLTTF.Font
	MiTexto: new list of Obj_lineas_texto
	texto_actual: Obj_lineas_texto
	_w:uint16=0
	_h:uint16=0
	
	init 
		MiTexto= new list of Obj_lineas_texto
		
	construct (referencia:int,posicion_x:int,posicion_y:int,w:int,h:int,texto:string)
		this.Ref=referencia
		this.Posicion_x=posicion_x
		this.Posicion_y=posicion_y
		this.Nombre="Etiqueta"
		this.Texto=texto
		
		this.ColorTexto.b=0
		this.ColorTexto.g=0
		this.ColorTexto.r=0
		this.TamanoTexto=15
		
		this.ColorControl.r=23
		this.ColorControl.g=223
		this.ColorControl.b=222
		this.ColorControl.unused=150
		
		this.ColorBorde.r=0
		this.ColorBorde.g=0
		this.ColorBorde.b=0
		this.ColorBorde.unused=255
		
		
		this.sr.x = 0;                   
		this.sr.y = 0;                 
		this.dr.x = (int16)posicion_x;  
		this.dr.y =(int16)posicion_y;
		  
		this.sr.w = (uint16)w;    
		this.sr.h= (uint16)h;
		this.dr.w = this.sr.w;                   
		this.dr.h = this.sr.h;
		_w=(uint16)w
		_h=(uint16)h
		set_Texto(texto)
		
		sdlk.genera_elemento(this)
		
		this.izq_pulsado.connect(control_pulsado)
		this.soltado.connect(control_soltado)
		
	def override set_Texto(nuevotexto:string)
		MiTexto.clear()
		Fuente= new Font(dd+"datuak/KatamotzIkasi.ttf",this.TamanoTexto)
		var contador1=0
		var contador2=0
		var posicion_anterior=0
		var ancho_maximo=0
		s:string
		if nuevotexto=="" do nuevotexto=" "
		contador2= busca_char(nuevotexto,'\n',contador2,true)
		if contador2<0 do contador2=ultima(nuevotexto)
		Fuente= new Font(dd+"datuak/KatamotzIkasi.ttf",this.TamanoTexto)
		while contador1!=-1
			texto_actual=  new Obj_lineas_texto
			s=toma_cadena(nuevotexto,contador1,contador2+1)
			s=s.replace("\n","")
			texto_actual.linea_imagen = Fuente.render_blended_utf8(s,this.ColorTexto)

			this.srt.x = 0;
			this.srt.y = 0;                 
			this.srt.w = (uint16)texto_actual.linea_imagen.w;    
			this.srt.h= (uint16)texto_actual.linea_imagen.h
			
			if (_w==0) and (_h==0)
				
				this.dr.w = (uint16)texto_actual.linea_imagen.w+20;    
				this.sr.w = (uint16)texto_actual.linea_imagen.w+20;    
				this.sr.h= (uint16)texto_actual.linea_imagen.h+10
				this.dr.h= (uint16)texto_actual.linea_imagen.h+10

			
			this.drt.x = (int16)this.Posicion_x +10;         
			this.drt.y =(int16)this.Posicion_y +posicion_anterior+10;  
			this.drt.w =(int16)this.srt.w;                  
			this.drt.h = (int16)this.srt.h;
			if this.drt.w>ancho_maximo  do  ancho_maximo=this.drt.w
			
			texto_actual.linea_texto=s
			texto_actual.linea_srt=this.srt
			texto_actual.linea_drt=this.drt
			MiTexto.add(texto_actual)
			
			contador1= busca_char(nuevotexto,'\n',contador1+1,true)
			contador2= busca_char(nuevotexto,'\n',contador2+1,true)
			if contador2==-1 do contador2=ultima(nuevotexto)
			posicion_anterior+=this.drt.h
			
		this.Texto = nuevotexto;
		this.dr.h=posicion_anterior+15
		this.dr.w=ancho_maximo+15
	def override get_Texto():string
		return this.Texto
		
	def override set_ColorBorde(r:uchar,g:uchar,b:uchar,a:uchar)
		this.ColorBorde.r=r
		this.ColorBorde.g=g
		this.ColorBorde.b=b
		this.ColorBorde.unused=a
		
	def set_ColorTexto(color:Color)
		Fuente= new Font(dd+"datuak/KatamotzIkasi.ttf",this.TamanoTexto)
		this.ColorTexto=color
		MiTexto.clear()
		set_Texto(this.Texto)
	
	def set_ColorTexto_rgb(r:uchar,g:uchar,b:uchar)
		Fuente= new Font(dd+"datuak/KatamotzIkasi.ttf",this.TamanoTexto)
		this.ColorTexto.r=r
		this.ColorTexto.g=g
		this.ColorTexto.b=b
		MiTexto.clear()
		set_Texto(this.Texto)
	
	
	
	def set_TamanoTexto(tamano:int)
		this.TamanoTexto=tamano
		MiTexto.clear()
		set_Texto(this.Texto)
	
	def override set_Posicion(x:int,y:int)
		this.dr.x=(int16)x
		this.dr.y=(int16)y
		this.drt.x=(int16)x+10
		this.drt.y=(int16)y+10

	def override set_Posicion_y(y:int)
		this.dr.y=(int16)y
		this.Posicion_y=(int16)y
		this.drt.y=(int16)y+10
		set_Texto(this.Texto)
		
	def override set_Posicion_x(x:int)
		this.dr.x=(int16)x
		this.Posicion_x=(int16)x
		this.drt.x=(int16)x+10
		set_Texto(this.Texto)
		
	def override get_Posicion_x(): int 
		return (int)this.dr.x
	
	def override get_Posicion_y(): int 
		return (int)this.dr.y


	def override get_Ancho(): int 
		return this.dr.w

	def override get_Alto(): int 
		return this.dr.h
		
			
	def override set_ColorControl(R:uchar,G:uchar,B:uchar,A:uchar)
		MiTexto.clear()
		this.ColorControl.r=R
		this.ColorControl.g=G
		this.ColorControl.b=B
		this.ColorControl.unused=A
		set_Texto(this.Texto)
		
		
	def control_soltado(c:Control)
		var a=c.Index
		if sdlk.lista_general[a].Arrastrable
			sdlk.lista_general[a].Tomado=false
	
	// evento pulsado
	def control_pulsado(c:Control)
		var a=c.Index
		if sdlk.lista_general[a].Arrastrable
			sdlk.lista_general[a].Tomado=true
			this.Tomado_x =sdlk.Puntero_x- this.dr.x //indica el lugar xy desde donde se engancha el boton
			this.Tomado_y = sdlk.Puntero_y - this.dr.y // para poder ser arrastrado manteniendo este punto.
			c.Arrastrado=true
		sdlk.Focus=c.Index
			
	def override pinta ()
		if this.Visible
			if (this.Tomado) 
			
				
				this.dr.x=(int16)sdlk.Puntero_x-(int16)this.Tomado_x
				if (this.dr.x+this.dr.w <0) do this.dr.x=5
				if (this.dr.x  >sdlk.Ancho) do this.dr.x=sdlk.Ancho-(int16)this.dr.w-5
				
				this.dr.y=(int16)sdlk.Puntero_y-(int16)this.Tomado_y
				if (this.dr.y+this.dr.h <0) do this.dr.y=5
				if (this.dr.y>sdlk.Alto) do this.dr.y=sdlk.Alto-(int16)this.dr.h-5
				
				this.Posicion_x=this.dr.x
				this.Posicion_y=this.dr.y
				
				var anterior=10
				for var i =0 to (ultimo_de_lista(MiTexto))
					MiTexto[i].linea_drt.x = (int16)(this.dr.x +10);         
					MiTexto[i].linea_drt.y = (int16)(this.dr.y +anterior);         
					anterior+=MiTexto[i].linea_drt.h
			
			if this.Fondo_Visible
				if this.Borde
					//dibuja rectangulo con borde
					sdlk.Rectangulo_redondo_borde(screen,this.dr,11,this.ColorControl.r,this.ColorControl.g,this.ColorControl.b,this.ColorControl.unused,
					this.ColorBorde.r,this.ColorBorde.g,this.ColorBorde.b,this.ColorBorde.unused)
				else
					//Dibuja rectangulo sin borde
					sdlk.Rectangulo_redondo(screen,this.dr,11,this.ColorControl.r,this.ColorControl.g,this.ColorControl.b,this.ColorControl.unused)	
			if this.TextoVisible
				for var i =0 to (ultimo_de_lista(MiTexto))
					MiTexto[i].linea_imagen.blit(MiTexto[i].linea_srt, screen, MiTexto[i].linea_drt)




class Boton: Control
	color:SDL.Color
	Fuente:SDLTTF.Font
	MiTexto: new list of Obj_lineas_texto
	texto_actual: Obj_lineas_texto
	pasando:bool=false
	tempo:int=0
	_w:uint16=0
	_h:uint16=0
	
	init 
		MiTexto= new list of Obj_lineas_texto
		
	construct (referencia:int,posicion_x:int,posicion_y:int,w:int,h:int,texto:string)
		this.Ref=referencia
		this.Posicion_x=posicion_x
		this.Posicion_y=posicion_y
		this.Nombre="Boton"
		this.Texto=texto
		this.Arrastrable=false
		
		this.ColorTexto.b=255
		this.ColorTexto.g=255
		this.ColorTexto.r=255
		this.TamanoTexto=20
		
		this.ColorControl.r=150
		this.ColorControl.g=150
		this.ColorControl.b=150
		this.ColorControl.unused=255
		
		this.sr.x = 0;                   
		this.sr.y = 0;
		this.sr.w = (uint16)w;    
		this.sr.h= (uint16)h;
		this.dr.x = (int16)posicion_x;  
		this.dr.y =(int16)posicion_y;  
		this.dr.w = this.sr.w;                   
		this.dr.h = this.sr.h;
		Pulsado=0
		_w=(uint16)w
		_h=(uint16)h
		set_texto(texto)
		
		
		sdlk.genera_elemento(this)
		//eventos
		this.izq_pulsado.connect(control_pulsado)
		this.soltado.connect(control_soltado)
		this.sobre.connect(control_sobre)
		
	def set_texto(nuevotexto:string)
		MiTexto.clear()
		Fuente= new Font(dd+"datuak/KatamotzIkasi.ttf",this.TamanoTexto)
		var contador1=0
		var contador2=0
		var posicion_anterior=0
		s:string
		contador2= busca_char(nuevotexto,'\n',contador2,true)
		if contador2<0 do contador2=ultima(nuevotexto)
		Fuente= new Font(dd+"datuak/KatamotzIkasi.ttf",this.TamanoTexto)
		while contador1!=-1
			texto_actual=  new Obj_lineas_texto
			s=toma_cadena(nuevotexto,contador1,contador2+1)
			s=s.replace("\n","")
			texto_actual.linea_imagen = Fuente.render_blended_utf8(s,this.ColorTexto)

			this.srt.x = 0;
			this.srt.y = 0;                 
			this.srt.w = (uint16)texto_actual.linea_imagen.w;    
			this.srt.h= (uint16)texto_actual.linea_imagen.h
			
			if (_w==0) and (_h==0)
				
				this.dr.w = (uint16)texto_actual.linea_imagen.w+20;    
				this.sr.w = (uint16)texto_actual.linea_imagen.w+20;    
				this.sr.h= (uint16)texto_actual.linea_imagen.h+10
				this.dr.h= (uint16)texto_actual.linea_imagen.h+10

			
			this.drt.x = (int16)this.Posicion_x +10;         
			this.drt.y =(int16)this.Posicion_y +posicion_anterior+10;  
			this.drt.w =(int16)this.srt.w;                  
			this.drt.h = (int16)this.srt.h;
			
			texto_actual.linea_texto=s
			texto_actual.linea_srt=this.srt
			texto_actual.linea_drt=this.drt
			MiTexto.add(texto_actual)
			
			contador1= busca_char(nuevotexto,'\n',contador1+1,true)
			contador2= busca_char(nuevotexto,'\n',contador2+1,true)
			if contador2==-1 do contador2=ultima(nuevotexto)
			posicion_anterior+=this.drt.h
			
		this.Texto = nuevotexto;
	
	def set_ColorTexto(color:Color)
		Fuente= new Font(dd+"datuak/KatamotzIkasi.ttf",this.TamanoTexto)
		this.ColorTexto=color
		MiTexto.clear()
		set_texto(this.Texto)
		
	def set_TamanoTexto(tamano:int)
		this.TamanoTexto=tamano
		MiTexto.clear()
		set_texto(this.Texto)
	
	def override set_Posicion(x:int,y:int)
		this.dr.x=(int16)x
		this.dr.y=(int16)y
		this.drt.x=(int16)x+10
		this.drt.y=(int16)y+10

	def override set_Posicion_y(y:int)
		this.dr.y=(int16)y
		this.drt.y=(int16)y+10

	def override set_Posicion_x(x:int)
		this.dr.x=(int16)x
		this.drt.x=(int16)x+10
	
	def override get_Posicion_x(): int 
		return this.dr.x
	
	def override get_Posicion_y(): int 
		return this.dr.y
		
	
			
	def set_ColorControl(R:uchar,G:uchar,B:uchar,A:uchar)
		MiTexto.clear()
		this.ColorControl.r=R
		this.ColorControl.g=G
		this.ColorControl.b=B
		this.ColorControl.unused=A
		set_texto(this.Texto)
		
		
	// evento pulsado
	def control_pulsado (c:Control)
		sdlk.Focus=c.Index
		Pulsado=1
		this.pasando=false
		c.Arrastrado=true
	//evento soltado
	def control_soltado (c:Control)
		Pulsado=0
		this.pasando=false
	def control_sobre(c:Control)
		this.pasando=true
		
	
		
	def override pinta ()
		if this.Visible
			
			if Pulsado==1 do sdlk.Boton_redondo(screen,this.dr,11,this.ColorControl.r+20,this.ColorControl.g,this.ColorControl.b,this.ColorControl.unused)
				
			if Pulsado==0 
				if this.pasando
					sdlk.Boton_redondo(screen,this.dr,11,this.ColorControl.r-20,this.ColorControl.g,this.ColorControl.b,this.ColorControl.unused)
					this.pasando=false
				else
					sdlk.Boton_redondo(screen,this.dr,11,this.ColorControl.r-20,this.ColorControl.g-20,this.ColorControl.b-20,this.ColorControl.unused)
					this.pasando=false
					
			if this.TextoVisible
				for var i =0 to (ultimo_de_lista(MiTexto))
					MiTexto[i].linea_imagen.blit(MiTexto[i].linea_srt, screen, MiTexto[i].linea_drt)
			
			



class Interruptor: Control
	color:SDL.Color
	Fuente:SDLTTF.Font
	MiTexto: new list of Obj_lineas_texto
	texto_actual: Obj_lineas_texto
	pasando:bool=false
	
	construct (referencia:int,posicion_x:int,posicion_y:int,texto:string)
		this.Ref=referencia
		this.Posicion_x=posicion_x
		this.Posicion_y=posicion_y
		this.Nombre="Interruptor"
		this.Texto=texto
		
		this.ColorTexto.b=255
		this.ColorTexto.g=255
		this.ColorTexto.r=255
		this.TamanoTexto=15
		set_texto(texto)
		
		this.ColorControl.r=23
		this.ColorControl.g=122
		this.ColorControl.b=102
		this.ColorControl.unused=255
		
		this.sr.x = 0;                   
		this.sr.y = 0;                 
		this.dr.x = (int16)posicion_x;  
		this.dr.y =(int16)posicion_y;  
		
		sdlk.genera_elemento(this)
		this.izq_pulsado.connect(control_pulsado)
		this.soltado.connect(control_soltado)
		
	def set_texto(nuevotexto:string)
		Fuente= new Font(dd+"datuak/KatamotzIkasi.ttf",this.TamanoTexto)
		this.Imagen_1 = Fuente.render_blended_utf8(nuevotexto,this.ColorTexto)
		
		this.srt.x = 0;
		this.srt.y = 0;                 
		this.srt.w = (uint16)this.Imagen_1.w;    
		this.srt.h= (uint16)this.Imagen_1.h

		this.drt.x = (int16)this.Posicion_x +50;         
		this.drt.y =(int16)this.Posicion_y +10;  
		this.drt.w =(int16)this.srt.w;                  
		this.drt.h = (int16)this.srt.h
		
		this.dr.w =(int16)this.Imagen_1.w +60
		this.dr.h = (int16)this.Imagen_1.h +20

		this.sr.w =(int16)this.Imagen_1.w +60
		this.sr.h = (int16)this.Imagen_1.h +20
		
		this.Texto = nuevotexto;
	
		
	def set_ColorTexto(color:Color)
		Fuente= new Font(dd+"datuak/KatamotzIkasi.ttf",this.TamanoTexto)
		this.ColorTexto=color
		set_texto(this.Texto)
		
	def set_TamanoTexto(tamano:int)
		this.TamanoTexto=tamano
		set_texto(this.Texto)
	
	def override set_Posicion(x:int,y:int)
		this.dr.x=(int16)x
		this.dr.y=(int16)y
		this.drt.x=(int16)x+10
		this.drt.y=(int16)y+10

	def override set_Posicion_y(y:int)
		this.dr.y=(int16)y
		this.drt.y=(int16)y+10

	def override set_Posicion_x(x:int)
		this.dr.x=(int16)x
		this.drt.x=(int16)x+10
	
	def override get_Posicion_x(): int 
		return this.dr.x
	def override get_Posicion_y(): int 
		return this.dr.y
		
		
	def set_ColorControl(R:uchar,G:uchar,B:uchar,A:uchar)
		this.ColorControl.r=R
		this.ColorControl.g=G
		this.ColorControl.b=B
		this.ColorControl.unused=A
		set_texto(this.Texto)
	
	def control_soltado(c:Control)
		pass
	
		
	// evento pulsado
	def control_pulsado(c:Control)
		this.Valor_bool= not this.Valor_bool
		sdlk.Focus=c.Index
		
		
	def override pinta ()
		
		if this.Visible
			sdlk.Rectangulo_redondo(screen,this.dr,11,this.ColorControl.r,this.ColorControl.g,this.ColorControl.b,this.ColorControl.unused)
			if this.Valor_bool
				Circle.outline_rgba(screen,this.dr.x+20,this.drt.y+this.drt.h/2,10+this.drt.h/10,this.ColorControl.r-50,this.ColorControl.g-50,this.ColorControl.b-50,this.ColorControl.unused)
				Circle.fill_rgba(screen,this.dr.x+20,this.drt.y+this.drt.h/2,6+this.drt.h/10,this.ColorControl.r-50,this.ColorControl.g-50,this.ColorControl.b-50,this.ColorControl.unused)
			else
				Circle.outline_rgba(screen,this.dr.x+20,this.drt.y+this.drt.h/2,10+this.drt.h/10,this.ColorControl.r-50,this.ColorControl.g-50,this.ColorControl.b-50,this.ColorControl.unused)
				Circle.outline_rgba(screen,this.dr.x+20,this.drt.y+this.drt.h/2,6+this.drt.h/10,this.ColorControl.r-50,this.ColorControl.g-50,this.ColorControl.b-50,this.ColorControl.unused)
			
			if this.TextoVisible
				this.Imagen_1.blit(this.srt, screen, this.drt)
		
			
			

//Selector de Textos

class Obj_lineas_texto_selector: Object
	linea_texto: string
	linea_imagen:  SDL.Surface
	linea_srt:  SDL.Rect
	linea_drt: SDL.Rect
	linea_dr: SDL.Rect
	
class Selector: Control
	color:SDL.Color
	Fuente:SDLTTF.Font
	Lista: new list of Obj_lineas_texto_selector
	texto_actual: Obj_lineas_texto_selector
	posicion_anterior:int16
	seleccion:int
	Mi_lista_str:new list of string
	Seleccionado:new Obj_lineas_texto_selector
	Despliege_arriba:bool=false
	ancho_maximo:int16
	
	
	init 
		Lista= new list of Obj_lineas_texto_selector
		Mi_lista_str=new list of string
		
	construct (referencia:int,posicion_x:int,posicion_y:int,despliege_arriba:bool,lista_str:list of string)
		Seleccionado=new Obj_lineas_texto_selector
		Mi_lista_str.add_all(lista_str)
		if tamano_de_lista(lista_str)<1
			lista_str.add("Elemento_0")
		
		Seleccionado.linea_texto=(lista_str[0])
		Despliege_arriba=despliege_arriba
		this.Ref=referencia
		this.Posicion_x=posicion_x
		this.Posicion_y=posicion_y
		this.Nombre="Selector"
		this.TextoVisible=false
		
		this.ColorTexto.b=255
		this.ColorTexto.g=255
		this.ColorTexto.r=255
		this.TamanoTexto=15
		set_Lista(lista_str)
		
		this.ColorControl.r=123
		this.ColorControl.g=123
		this.ColorControl.b=252
		this.ColorControl.unused=255
		
		this.sr.x = 0;                   
		this.sr.y = 0;                 
		
		this.dr.x = (int16)posicion_x;  
		this.dr.y =(int16)posicion_y;  
		
		sdlk.genera_elemento(this)
		
		this.izq_pulsado.connect(evento_pulsado)
		this.soltado.connect(evento_soltado)
		this.izq_sobre.connect(evento_izq_sobre)
		
	def set_Lista(lista_str:list of string)
		Lista.clear()
		Fuente= new Font(dd+"datuak/KatamotzIkasi.ttf",this.TamanoTexto)
		texto_actual=  new Obj_lineas_texto_selector
		texto_actual.linea_texto=Seleccionado.linea_texto
		Seleccionado.linea_imagen = Fuente.render_blended_utf8(texto_actual.linea_texto,this.ColorTexto)
		
		Seleccionado.linea_srt.x = 0;
		Seleccionado.linea_srt.y = 0;                 
		Seleccionado.linea_srt.w = (uint16)Seleccionado.linea_imagen.w;    
		Seleccionado.linea_srt.h= (uint16)Seleccionado.linea_imagen.h
		
		Seleccionado.linea_drt.x = (int16)this.Posicion_x +10;
		Seleccionado.linea_drt.y =(int16)this.Posicion_y +5;  
		Seleccionado.linea_drt.w =(int16)Seleccionado.linea_srt.w;                  
		Seleccionado.linea_drt.h = (int16)Seleccionado.linea_srt.h;
		
		Seleccionado.linea_dr.x = (int16)this.Posicion_x ;
		Seleccionado.linea_dr.y =(int16)this.Posicion_y;  
		Seleccionado.linea_dr.w =(int16)Seleccionado.linea_srt.w+20;                  
		Seleccionado.linea_dr.h = (int16)Seleccionado.linea_srt.h+5;
		
		ancho_maximo= (int16)Seleccionado.linea_dr.w+10;
		posicion_anterior= (int16)Seleccionado.linea_dr.h+10;
		for var i=0 to ultimo_de_lista(lista_str)
			if lista_str[i]!= Seleccionado.linea_texto // Si es el seleccionado no se muestra en la lista desplegable
				texto_actual=  new Obj_lineas_texto_selector
				texto_actual.linea_texto=lista_str[i]
				
				texto_actual.linea_imagen = Fuente.render_blended_utf8(texto_actual.linea_texto,this.ColorTexto)
				
				texto_actual.linea_srt.x = 0;
				texto_actual.linea_srt.y = 0;                 
				texto_actual.linea_srt.w = (uint16)texto_actual.linea_imagen.w;    
				texto_actual.linea_srt.h= (uint16)texto_actual.linea_imagen.h
				
				texto_actual.linea_drt.x = (int16)this.Posicion_x +10;
				if not Despliege_arriba
					texto_actual.linea_drt.y =(int16)this.Posicion_y +posicion_anterior+5;  
				else
					texto_actual.linea_drt.y =(int16)this.Posicion_y -posicion_anterior+5;  
				
				texto_actual.linea_drt.w =(int16)texto_actual.linea_srt.w;                  
				texto_actual.linea_drt.h = (int16)texto_actual.linea_srt.h;
			
				
				texto_actual.linea_dr.x = (int16)this.Posicion_x ;
				if not Despliege_arriba
					texto_actual.linea_dr.y =(int16)this.Posicion_y +posicion_anterior;  
				else
					texto_actual.linea_dr.y =(int16)this.Posicion_y - posicion_anterior;  
				texto_actual.linea_dr.w =(int16)texto_actual.linea_srt.w+20;                  
				texto_actual.linea_dr.h = (int16)texto_actual.linea_srt.h+5;
				
				Lista.add(texto_actual)
				if (int16)texto_actual.linea_dr.w+10>ancho_maximo do ancho_maximo= (int16)texto_actual.linea_dr.w+10  ;
				posicion_anterior+=(int16)texto_actual.linea_drt.h+10
		
		
		this.Texto = Lista[0].linea_texto;
		this.sr.h=Seleccionado.linea_drt.h+5
		this.sr.w=Seleccionado.linea_drt.w+20
		this.dr.h=Seleccionado.linea_drt.h+5
		this.dr.w=Seleccionado.linea_drt.w+20
		
		 
	
	def set_ColorTexto(color:Color)
		
		Fuente= new Font(dd+"datuak/KatamotzIkasi.ttf",this.TamanoTexto)
		this.ColorTexto=color
		Lista.clear()
		set_Lista(Mi_lista_str)
			
	def set_TamanoTexto(tamano:int)
		this.TamanoTexto=tamano
		Lista.clear()
		set_Lista(Mi_lista_str)
			
		
	def override set_Posicion(x:int,y:int)
		this.dr.x=(int16)x
		this.dr.y=(int16)y
		this.drt.x=(int16)x+10
		this.drt.y=(int16)y+10

	def override set_Posicion_y(y:int)
		this.dr.y=(int16)y
		this.drt.y=(int16)y+10

	def override set_Posicion_x(x:int)
		this.dr.x=(int16)x
		this.drt.x=(int16)x+10
	
	def override get_Posicion_x(): int 
		return this.dr.x
	
	def override get_Posicion_y(): int 
		return this.dr.y
		
			
	def set_ColorControl(R:uchar,G:uchar,B:uchar,A:uchar)
		Lista.clear()
		this.ColorControl.r=R
		this.ColorControl.g=G
		this.ColorControl.b=B
		this.ColorControl.unused=A
		set_Lista(Mi_lista_str)
	
	//evento
	def evento_pulsado(c:Control)
		var a=c.Index
		sdlk.Focus=c.Index
		if not this.TextoVisible
			this.TextoVisible=true
			if not Despliege_arriba
				this.dr.h=posicion_anterior
				this.dr.w=ancho_maximo
				
			else
				this.dr.y=(int16)Seleccionado.linea_dr.y-posicion_anterior+(int16)Seleccionado.linea_dr.h
				this.dr.h=posicion_anterior
				this.dr.w=ancho_maximo
			
	def evento_izq_sobre(c:Control)
		var a=c.Index
		if not Despliege_arriba
			seleccion=((sdlk.Puntero_y - this.dr.y+ Seleccionado.linea_dr.h+5)/ (Seleccionado.linea_dr.h+5))-2
		else
			seleccion=tamano_de_lista(Lista)-((sdlk.Puntero_y - this.dr.y)/ (Seleccionado.linea_dr.h+5))-1
		
	def evento_soltado(c:Control)
		var a=c.Index
		if this.TextoVisible
			this.TextoVisible=false
			if not Despliege_arriba
				seleccion=((sdlk.Puntero_y - this.dr.y+Seleccionado.linea_dr.h+5)/ (Seleccionado.linea_dr.h+5))-2
			else
				seleccion=tamano_de_lista(Lista)-((sdlk.Puntero_y - this.dr.y)/ (Seleccionado.linea_dr.h+5))-1
			// vuelta a control normalizado
			this.dr.h=Seleccionado.linea_dr.h
			this.dr.y=(int16)Seleccionado.linea_dr.y
			
			if (seleccion>=0) and (seleccion<=ultimo_de_lista(Lista))
			 
				Seleccionado.linea_texto=Lista[seleccion].linea_texto
				set_Lista(Mi_lista_str)
	
	def override pinta ()
		if this.Visible
			
			
			if this.TextoVisible
				sdlk.Rectangulo_redondo(screen,Seleccionado.linea_dr,11,this.ColorControl.r,this.ColorControl.g,this.ColorControl.b,this.ColorControl.unused)
				Seleccionado.linea_imagen.blit(Seleccionado.linea_srt, screen, Seleccionado.linea_drt)
				for var i =0 to (ultimo_de_lista(Lista))
					if i==seleccion
						sdlk.Rectangulo_redondo(screen,Lista[i].linea_dr,11,this.ColorControl.r,this.ColorControl.g,this.ColorControl.b,this.ColorControl.unused)
					else
						sdlk.Rectangulo_redondo(screen,Lista[i].linea_dr,11,this.ColorControl.r-50,this.ColorControl.g-50,this.ColorControl.b-50,this.ColorControl.unused)
					
					Lista[i].linea_imagen.blit(Lista[i].linea_srt, screen, Lista[i].linea_drt)
				
					
			else
				sdlk.Rectangulo_redondo(screen,Seleccionado.linea_dr,11,this.ColorControl.r,this.ColorControl.g,this.ColorControl.b,this.ColorControl.unused)
				Seleccionado.linea_imagen.blit(Seleccionado.linea_srt, screen, Seleccionado.linea_drt)
				


class Video: Control
	color:SDL.Color
	frame_kopurua:int=0
	frame_actual:int=0
	playing:bool=false
	archivo_audio: SDL.RWops
	audio: SDLMixer.Chunk 
	canala: SDLMixer.Channel
	audio_presente:bool=false
	video_presente:bool=false
	construct (referencia:int,posicion_x:int,posicion_y:int,ancho:int,alto:int,archivo:string)
		this.Ref=referencia
		this.Posicion_x=posicion_x
		this.Posicion_y=posicion_y
		this.Nombre="Video"
		this.TextoVisible=false
		
		this.ColorControl.r=123
		this.ColorControl.g=123
		this.ColorControl.b=252
		this.ColorControl.unused=255
		try
			this.Imagen_0=SDLImage.load ( archivo+".gif")
			video_presente=true
		except
			video_presente=true
		try
			archivo_audio= new SDL.RWops.from_file (archivo+".ogg","rb")
			audio= new SDLMixer.Chunk.WAV (archivo_audio,-1)
			audio_presente=true
		except
			audio_presente=false
		
		if not video_presente	
			pass
			print"no pudo cargarse video. Esto puede crear errores"
		else
			frame_kopurua=this.Imagen_0.w/ancho
			frame_actual=0
			
			this.sr.x = 0;                   
			this.sr.y = 0;                 
			this.sr.w = (int16)ancho;                   
			this.sr.h = (int16)alto;                 
			
			this.dr.x = (int16)posicion_x;  
			this.dr.y =(int16)posicion_y;  
			this.dr.w = (int16)ancho;  
			this.dr.h =(int16)alto;  
			
			sdlk.genera_elemento(this)
			
			this.izq_pulsado.connect(evento_pulsado)
			this.soltado.connect(evento_soltado)
	
	def set_Tamano(w:int,h:int)
	
		
		ha:double
		wa:double =(double)(w)/this.sr.w
		ha=(double)h/(double)this.sr.h

		this.Imagen_0= RotoZoom.zoom(this.Imagen_0,wa,ha,1)
		this.sr.w = (uint16)this.Imagen_0.w/(uint16)frame_kopurua;
		this.sr.h = (uint16)this.Imagen_0.h;
		this.dr.w = (uint16)this.Imagen_0.w/(uint16)frame_kopurua;
		this.dr.h = (uint16)this.Imagen_0.h;
		this.drt.x = (int16)this.dr.x +(this.sr.w/2)-(this.srt.w/2);         
		this.drt.y =(int16)this.dr.y+(this.sr.h/2)-(this.srt.w/2);
		this.Mascara=sdlk.convertir_en_mascara(this.Imagen_0)
		
	def play()
		playing=true
		frame_actual=0
		if audio_presente do canala.play(audio,0)
		
	def stop()
		playing=false
		frame_actual=0
		
		
	def override set_Posicion(x:int,y:int)
		this.dr.x=(int16)x
		this.dr.y=(int16)y
		this.drt.x=(int16)x+10
		this.drt.y=(int16)y+10

	def override set_Posicion_y(y:int)
		this.dr.y=(int16)y
		this.drt.y=(int16)y+10

	def override set_Posicion_x(x:int)
		this.dr.x=(int16)x
		this.drt.x=(int16)x+10
	
	def override get_Posicion_x(): int 
		return this.dr.x
	
	def override get_Posicion_y(): int 
		return this.dr.y
		
			
	def set_ColorControl(R:uchar,G:uchar,B:uchar,A:uchar)
		this.ColorControl.r=R
		this.ColorControl.g=G
		this.ColorControl.b=B
		this.ColorControl.unused=A
	
	// evento soltado
	def evento_soltado(c:Control)
		if c.Arrastrable
			c.Tomado=false
	
	// evento pulsado
	def evento_pulsado(c:Control)
		this.Tomado_x = sdlk.Puntero_x- this.dr.x //indica el lugar xy desde donde se engancha el boton
		this.Tomado_y = sdlk.Puntero_y - this.dr.y // para poder ser arrastrado manteniendo este punto.
		if (c.Arrastrable) 
			c.Tomado=true
		sdlk.Focus=c.Index
			

		
	def override pinta ()
		if this.Visible
			if Tomado
				// si esta tomado muevelo con el raton, cuidando que no se salga de los bordes de la ventana
				this.dr.x=(int16)sdlk.Puntero_x-(int16)this.Tomado_x
				if (this.dr.x+this.dr.w <0) do this.dr.x=5
				if (this.dr.x  >sdlk.Ancho) do this.dr.x=sdlk.Ancho-(int16)this.dr.w-5
				this.dr.y=(int16)sdlk.Puntero_y-(int16)this.Tomado_y
				if (this.dr.y+this.dr.h <0) do this.dr.y=5
				if (this.dr.y  >sdlk.Alto) do this.dr.y= sdlk.Alto-(int16)this.dr.h-5
				
			if playing
				this.Imagen_0.blit (sr, screen, dr)
				sr.x=sr.x+(int16)sr.w
				if (sr.x>(this.Imagen_0.w)) 
					sr.x=0
					playing=false
			if not playing
				this.Imagen_0.blit (sr, screen, dr)
		
		
		
		
// La clase que define la Entrada de caracteres en una caja 					
class Entrada: Control
	color:SDL.Color
	Fuente:SDLTTF.Font
	texto_actual: new Obj_lineas_texto
	posicion_x: new list of int
	posicion_y: new list of int
	tempo:int
	Longmax:int
	Maxlin:int
	col:int=0
	lin:int=0
	pos:int=0
	
	multiline:bool=false
	
	MiTexto:new list of Obj_lineas_texto
	init 
		MiTexto= new list of Obj_lineas_texto
	
	construct (referencia:int,posicionx:int16,posiciony:int16,ancho:uint16,alto:uint16,ml:bool)
		this.Ref=referencia
		this.Posicion_x=posicionx
		this.Posicion_y=posiciony
		this.Nombre="Entrada"
		this.Texto=""
		
		
		sdlk.genera_elemento(this)
		
		set_ColorTexto(0,0,0,255)
		set_ColorControl(20,90,200,255)
			
		multiline=ml
		this.tecleado.connect(control_tecleado)
		this.izq_pulsado.connect(control_pulsado)
		
		this.sr.x=0
		this.sr.y=0
		this.sr.w=ancho
		this.sr.h=alto
		
		this.dr.x=posicionx
		this.dr.y=posiciony
		this.dr.w=ancho
		this.dr.h=alto
		this.drt.x=posicionx
		this.drt.y=posiciony
		this.drt.w=ancho
		this.drt.h=alto

		posicion_y = new list of int
		
		posicion_x = new list of int
		posicion_x.add( (int)this.dr.x )
		posicion_y.add( (int)this.dr.y )
		
		tempo=0
		Longmax=-1
		Maxlin=-1
		
	def get_Texto (): string
		var r=""
		if not multiline
			if ultimo_de_lista(MiTexto)>=0
				r=MiTexto[0].linea_texto
		return r
	
	
	def set_Texto(nuevotexto:string)
		
		Fuente= new Font(dd+"datuak/KatamotzIkasi.ttf",this.TamanoTexto)
		
		MiTexto.clear()
		
		var contador1=0
		var contador2=0
		var posicion_anterior=0
		var s=""
		contador2= busca_char(nuevotexto,'\n',contador2,true)
		if contador2<0 do contador2=ultima(nuevotexto)
		while contador1!=-1
			texto_actual=  new Obj_lineas_texto
			s=toma_cadena(nuevotexto,contador1,contador2+1)
			s=s.replace("\n","")
			if s!="" do	texto_actual.linea_imagen = Fuente.render_blended_utf8(s,this.ColorTexto)
			if s=="" do texto_actual.linea_imagen = Fuente.render_blended_utf8(" ",this.ColorTexto)
			this.srt.x = 0;
			this.srt.y = 0;                 
			this.srt.w = (uint16)texto_actual.linea_imagen.w;    
			this.srt.h= (uint16)texto_actual.linea_imagen.h
			
			this.drt.x = (int16)this.Posicion_x ;         
			this.drt.y =(int16)this.Posicion_y +posicion_anterior;  
			this.drt.w =(int16)this.srt.w;                  
			this.drt.h = (int16)this.srt.h;
			
			texto_actual.linea_texto=s
			texto_actual.linea_srt=this.srt
			texto_actual.linea_drt=this.drt
			MiTexto.add(texto_actual)
			contador1= busca_char(nuevotexto,'\n',contador1+1,true)
			contador2= busca_char(nuevotexto,'\n',contador2+1,true)
			if contador2==-1 do contador2=ultima(nuevotexto)
			posicion_anterior+=this.drt.h
		
	def renderiza_mitexto()
		var posicion_anterior=0
		for var i=0 to ultimo_de_lista(MiTexto)
			Fuente= new Font(dd+"datuak/KatamotzIkasi.ttf",this.TamanoTexto)
			if MiTexto[i].linea_texto!="" do MiTexto[i].linea_imagen = Fuente.render_blended_utf8(MiTexto[i].linea_texto,this.ColorTexto)
			if MiTexto[i].linea_texto=="" do MiTexto[i].linea_imagen = Fuente.render_blended_utf8(" ",this.ColorTexto)
			
			this.srt.x = 0;
			this.srt.y = 0;                 
			this.srt.w = (uint16)MiTexto[i].linea_imagen.w;    
			this.srt.h= (uint16)MiTexto[i].linea_imagen.h
			
			this.drt.x = (int16)this.Posicion_x ;         
			this.drt.y =(int16)this.Posicion_y +posicion_anterior;  
			this.drt.w =(int16)this.srt.w;                  
			this.drt.h = (int16)this.srt.h;
			
			
			
			MiTexto[i].linea_srt=this.srt
			MiTexto[i].linea_drt=this.drt
			posicion_anterior+=this.drt.h

	def inserta(t:string)
		texto_actual= new Obj_lineas_texto
		texto_actual.linea_texto=""
		while (lin > ultimo_de_lista(MiTexto))
			MiTexto.add(texto_actual)
		if longitud(MiTexto[lin].linea_texto)<=col
			MiTexto[lin].linea_texto+=t
			col++
		else
			MiTexto[lin].linea_texto=toma_cadena (MiTexto[lin].linea_texto,0,col)+t+ toma_cadena (MiTexto[lin].linea_texto, col,longitud(MiTexto[lin].linea_texto ))
			col++
		renderiza_mitexto()
	
	
	def elimina()
		texto_actual= new Obj_lineas_texto
		texto_actual.linea_texto=""
		while (lin > ultimo_de_lista (MiTexto) )
			MiTexto.add(texto_actual)
			
		if (longitud(MiTexto[lin].linea_texto)==0) and (lin>0)
			MiTexto.remove_at(lin)
			lin--
			col=longitud(MiTexto[lin].linea_texto)
		else
			if col>0
				MiTexto[lin].linea_texto=toma_cadena (MiTexto[lin].linea_texto,0,col-1)+toma_cadena (MiTexto[lin].linea_texto,col,longitud(MiTexto[lin].linea_texto))
				col-- 
		renderiza_mitexto()
	
	def posicion_cursor_x():int
		var w=0
		var h=0
		var lugar=""
		var sum=0
		sum=this.dr.x
		if ultimo_de_lista(MiTexto)>=lin
			if longitud(MiTexto[lin].linea_texto)>0
				Fuente= new Font(dd+"datuak/KatamotzIkasi.ttf",this.TamanoTexto)
				lugar=toma_cadena(MiTexto[lin].linea_texto,0,col)
				Fuente.size_utf8(lugar,ref w,ref h)
				sum+=w
			
		return sum
		
	
	def posicion_cursor_y():int
		var w=0
		var h=0
		var sum=0
		sum=this.dr.y
		texto_actual= new Obj_lineas_texto
		texto_actual.linea_texto=""
		
		while (lin > ultimo_de_lista (MiTexto) )
			MiTexto.add(texto_actual)
		
		Fuente= new Font(dd+"datuak/KatamotzIkasi.ttf",this.TamanoTexto)
		Fuente.size_utf8("asdf",ref w,ref h)
		sum+=(h*lin)
		return sum
		
		
		
	// evento pulsado
	def control_tecleado(a:int,t:string)
		tempo=16
		if longitud(t)>3
			if (t=="Teclado:backspace")
				elimina()
					
			if (t=="Teclado:return") and (multiline==true)
				if (Maxlin>0) 
					if (Maxlin-1>lin)
						lin++
						col=0
				else
					lin++
					col=0
				
			if (t=="Teclado:left") 
				if col>0
					col--
				
				
			if (t=="Teclado:right") 
				if col<longitud( MiTexto[lin].linea_texto )
					col++
				
			if (t=="Teclado:up") 
				if lin>0
					if ultima(MiTexto[lin-1].linea_texto)<col
						col=longitud(MiTexto[lin-1].linea_texto)
					lin--
				
				
			if (t=="Teclado:down") 
				if lin<ultimo_de_lista(MiTexto)
					if ultima(MiTexto[lin+1].linea_texto)<col
						col=longitud(MiTexto[lin+1].linea_texto)
					lin++
					
		else
			
			if (Longmax<0)
				//introduce caracteres
				inserta(t)
			else
				if lin<=ultimo_de_lista(MiTexto)
					if ( Longmax >  ultima(MiTexto[lin].linea_texto))
						inserta(t)
			
	//evento
	def control_pulsado(c:Control)
		sdlk.Focus=c.Index
		
	def set_ColorControl(R:uchar,G:uchar,B:uchar,A:uchar)
		this.ColorControl.r=R
		this.ColorControl.g=G
		this.ColorControl.b=B
		this.ColorControl.unused=A

	def set_ColorTexto(R:uchar,G:uchar,B:uchar,A:uchar)
		this.ColorTexto.r=R
		this.ColorTexto.g=G
		this.ColorTexto.b=B
		this.ColorTexto.unused=A
	
	def set_TamanoTexto(tamano:int)
		this.TamanoTexto=tamano
		this.renderiza_mitexto()
		
	
	def override pinta ()
		if this.Visible
			colorborde:uint32
			tempo++
			colorborde=screen.format.map_rgba(00,00,150,250)
			
			//dibuja el rectangulo redondo del control
			sdlk.Rectangulo_redondo(screen,this.dr,11,this.ColorControl.r,this.ColorControl.g,this.ColorControl.b,this.ColorControl.unused)
			// escribe el cursor parpadeante
			if (sdlk.Focus==this.Index)
				if tempo>15
					Line.rgba_v (screen, (int16)posicion_cursor_x(),(int16)posicion_cursor_y(),(int16)posicion_cursor_y()+(int16)this.drt.h, 
						this.ColorTexto.r,this.ColorTexto.g,this.ColorTexto.b,this.ColorTexto.unused)
						
				if tempo>30 do tempo=0
				
			if this.TextoVisible
				for var i =0 to ultimo_de_lista(MiTexto)
					MiTexto[i].linea_imagen.blit(this.sr, screen, MiTexto[i].linea_drt)
					
					



class CuadradoRedondo: Control
	color:SDL.Color
	_w:uint16=0
	_h:uint16=0
	
		
	construct (referencia:int,posicion_x:int,posicion_y:int,w:int,h:int)
		this.Ref=referencia
		this.Posicion_x=posicion_x
		this.Posicion_y=posicion_y
		this.Nombre="CuadradoRedondo"
		
		this.ColorTexto.b=0
		this.ColorTexto.g=0
		this.ColorTexto.r=0
		this.TamanoTexto=15
		
		this.ColorControl.r=23
		this.ColorControl.g=223
		this.ColorControl.b=222
		this.ColorControl.unused=150
		
		this.sr.x = 0;                   
		this.sr.y = 0;                 
		this.dr.x = (int16)posicion_x;  
		this.dr.y =(int16)posicion_y;
		  
		this.sr.w = (uint16)w;    
		this.sr.h= (uint16)h;
		this.dr.w = this.sr.w;                   
		this.dr.h = this.sr.h;
		_w=(uint16)w
		_h=(uint16)h
		
		sdlk.genera_elemento(this)
		
		this.izq_pulsado.connect(control_pulsado)
		this.soltado.connect(control_soltado)
		
		
		
	
	def override set_Posicion(x:int,y:int)
		this.dr.x=(int16)x
		this.dr.y=(int16)y
		this.drt.x=(int16)x+10
		this.drt.y=(int16)y+10

	def override set_Posicion_y(y:int)
		this.dr.y=(int16)y
		this.Posicion_y=(int16)y
		this.drt.y=(int16)y+10
		
	def override set_Posicion_x(x:int)
		this.dr.x=(int16)x
		this.Posicion_x=(int16)x
		this.drt.x=(int16)x+10
		
	def override get_Posicion_x(): int 
		return (int)this.dr.x
	
	def override get_Posicion_y(): int 
		return (int)this.dr.y


	def override get_Ancho(): int 
		return this.dr.w

	def override get_Alto(): int 
		return this.dr.h
		
			
	def set_ColorControl(R:uchar,G:uchar,B:uchar,A:uchar)
		this.ColorControl.r=R
		this.ColorControl.g=G
		this.ColorControl.b=B
		this.ColorControl.unused=A
		
	def control_soltado(c:Control)
		var a=c.Index
		if sdlk.lista_general[a].Arrastrable
			sdlk.lista_general[a].Tomado=false
	
	// evento pulsado
	def control_pulsado(c:Control)
		var a=c.Index
		if sdlk.lista_general[a].Arrastrable
			sdlk.lista_general[a].Tomado=true
			this.Tomado_x =sdlk.Puntero_x- this.dr.x //indica el lugar xy desde donde se engancha el boton
			this.Tomado_y = sdlk.Puntero_y - this.dr.y // para poder ser arrastrado manteniendo este punto.
			c.Arrastrado=true
		if not c.FiguraFondo
			sdlk.Focus=c.Index
		else
			sdlk.Focus=-1
			
	def override pinta ()
		if this.Visible
			if this.Tomado
				this.dr.x=(int16)sdlk.Puntero_x-(int16)this.Tomado_x
				if (this.dr.x+this.dr.w <0) do this.dr.x=5
				if (this.dr.x >sdlk.Ancho) do this.dr.x=sdlk.Ancho-(int16)this.dr.w-5
				this.dr.y=(int16)sdlk.Puntero_y-(int16)this.Tomado_y
				if (this.dr.y+this.dr.h <0) do this.dr.y=5
				if (this.dr.y  >sdlk.Alto) do this.dr.y=sdlk.Alto-(int16)this.dr.h-5
				
			sdlk.Rectangulo_redondo(screen,this.dr,11,this.ColorControl.r,this.ColorControl.g,this.ColorControl.b,this.ColorControl.unused)
			



