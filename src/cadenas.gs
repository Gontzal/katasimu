uses
	Gee
lista_sp:string
letras_conacento: new array of string
letras_sinacento: new array of string
def toma_cadena (a:string,i :int,f:int=-1):string
	b: string
	
	var bu = new StringBuilder ();
	if f==-1
		bu.append_unichar(a.get_char(a.index_of_nth_char(i)))
		b=bu.str
	else
		if f>0
			var p=i
			while p<f
				bu.append_unichar(a.get_char(a.index_of_nth_char(p)))
				p++
			b=bu.str
		else
			b=""
	return b

def busca_char (a:string,b:char,i:int=0,adelante:bool=true):int
	
	var r=-1
	if adelante==true
		var c = i
		while c <= ultima(a)
			if a.get_char(a.index_of_nth_char(c))==b
				r=c
				break
			c++
	else
		var c = i
		while c >= 0
			if a.get_char(a.index_of_nth_char(c))==b
				r=c
				break
			c--
	
	return r 


def busca_cadena (a:string,b:string,i:int=0,adelante:bool=true):int
	var r=-1
	var coincide=0
	c:int =0
	if (longitud(b) == 0) or (longitud(a)==0)
		return r
	else
		if adelante==true
			for c = i to ultima(a)
				coincide=0
				for var h =0 to ultima(b)
					if a.get_char(a.index_of_nth_char(c+h))==b.get_char(b.index_of_nth_char(h))
						coincide++
					else
						break
						
				if coincide == longitud(b)
					r=c
					break
		else
			c=i
			while c >=0
				coincide=0
				for var h =0 to ultima(b)
					if a.get_char(a.index_of_nth_char(c+h))==b.get_char(b.index_of_nth_char(h))
						coincide++
					else
						break
				if coincide == longitud(b)
					r=c
					break
				c--
				
		return r
		 
def valida_sp(s:string):bool
	lista_sp="abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZáéíóúüÁÉÍÓÚÜÇç?¿=)(/!ª'¡º1234567890-.,;:_{}[]*+-<> "
	r:bool=false
	for var i=0 to ultima(s)
		var n= toma_cadena(s,i,i+1)
		if n in lista_sp 
			r=true
		else
			r= false
			
	return r
	
def longitud (a:string):int
	return a.char_count()

def ultima (a:string):int
	return (a.char_count()-1)

def transforma_sinacento(palabra:string):string
	var r=palabra
	letras_conacento={"á","é","í","ó","ú","ñ","ü"}
	letras_sinacento={"_a","_e","_i","_o","_u","_n","0u"}
	for var x=0 to 6
		if letras_conacento[x] in r
			r=r.replace(letras_conacento[x],letras_sinacento[x])
	return r

def transforma_conacento(palabra:string):string
	 var r=palabra
	letras_conacento={"á","é","í","ó","ú","ñ","ü"}
	letras_sinacento={"_a","_e","_i","_o","_u","_n","0u"}
	for var x=0 to 6
		if letras_sinacento[x] in r
			r=r.replace(letras_conacento[x],letras_conacento[x])
	return r

def ultimo_de_lista(s:Collection):int 
	return s.size-1
		
def tamano_de_lista(s:Collection):int 
	return s.size
	
def tostring(longi:int,numero:int):string
	var r1= ""
	var r2=""
	var l=0
	r1=numero.to_string()
	l=longi-r1.length
	if l>-1 do r2=string.nfill( l,'0')
	
	return r2+numero.to_string()
	
def desordena_lista_int (ref lista:list of int )
		for var i=0 to ultimo_de_lista(lista)
			var lugar=Random.int_range(0,ultimo_de_lista(lista))
			var a=lista[lugar]
			lista.remove_at(lugar)
			lista.add(a)

def desordena_lista_string (ref lista:list of string )
		for var i=0 to ultimo_de_lista(lista)
			var lugar=Random.int_range(0,ultimo_de_lista(lista))
			var a=lista[lugar]
			lista.remove_at(lugar)
			lista.add(a)
	

