uses Gee
uses SDL
uses SDLImage
uses SDLGraphics
uses SDLTTF
uses GLib

// Entzun zenbakia eta egokitu kopurua.1-10

class zenbakiak12 : Object
	imga: list of Imagen
	imgb: list of Imagen
	numero:Etiqueta
	casa2:CuadradoRedondo
	casa1:CuadradoRedondo
	casa0:CuadradoRedondo
	numeroa:Imagen
	igual:Imagen
	fin:bool=false
	comando:int=0
	solucion:int=0
	contador:int=1
	salir:Imagen
	continuar:Imagen
	zuzendu:Boton
	
	init 
		imga= new list of Imagen
		imgb= new list of Imagen
		
	def inicio()
		
		sdlk.muere_todo()
		fin=false
		comando=-10
		contador=1
		solucion= Random.int_range(1,11)
		imga.clear()
		imgb.clear()
		var archivo= iruditrans [ Random.int_range(0,ultimo_de_lista(iruditrans)) ]
		
		casa0= new CuadradoRedondo(0,70,170,160,160)
		casa0.set_ColorControl(100,150,150,255)
		casa0.Arrastrable=false
		casa0.FiguraFondo=true
		

		casa1= new CuadradoRedondo(0,400,30,160,480)
		casa1.set_ColorControl(100,150,150,255)
		casa1.Arrastrable=false
		casa1.FiguraFondo=true
		
		
		casa2= new CuadradoRedondo(0,600,210,160,160)
		casa2.set_ColorControl(10,150,150,255)
		casa2.Arrastrable=false
		casa2.FiguraFondo=true
		
		
		
		igual= new Imagen(0,250,200,dd+"irudiak/erabilgarri/berdin.png")
		igual.Arrastrable=false
		
		
		numeroa= new Imagen(0,80,180,dd+"irudiak/erabilgarri/musika.png")
		numeroa.Arrastrable=false
		numeroa.izq_pulsado.connect(on_escucha)
		
		for var i=0 to 10
			imgb.add( new Imagen (1, 610,220,dd+"irudiak/hitz_transparenteak/"+archivo))
			imgb[i].set_Tamano (100,100)
		
		
		numero= new Etiqueta(0,300,70,0,0," ")
		numero.Visible=false
		numero.set_ColorControl(100,150,100,255)
		numero.set_TamanoTexto(60)
		numero.Arrastrable=false
		
		
		salir= new Imagen(1,70,547,dd+"irudiak/erabilgarri/atzera.png")
		salir.set_Tamano(40,40)
		salir.izq_pulsado.connect(on_salir)
		
		continuar= new Imagen(1,120,547,dd+"irudiak/erabilgarri/Repetir.png")
		continuar.set_Tamano(40,40)
		continuar.izq_pulsado.connect(on_continuar)
		
		zuzendu= new Boton(1,320,550,0,0,"Zuzendu")
		zuzendu.izq_pulsado.connect(on_zuzendu)
		
		while not fin
			// toma eventos.
			sdlk.mira_sobre()
			while (SDL.Event.poll (out evento))== 1
				if evento.type == SDL.EventType.QUIT
					fin= true
					comando=-1
					break
				else
					sdlk.toma_eventos(evento)
			//pintar el fondo
			pinta_fondo()		
			// Realiza los cambios de juego necesarios
			sdlk.update_control()
			// Pinta los controles
			sdlk.pintar()
			SDL.Timer.delay(50)
		
		//SDL.Timer.delay(500)
		P.continua(comando)
		
	def pinta_fondo()
		Rectangle.fill_rgba(screen, 0, 0, sdlk.Ancho, sdlk.Alto, 255,210,151,255)
		dr:Rect= new Rect
		dr.x=10; dr.y=sdlk.Alto-60; dr.w=sdlk.Ancho-20; dr.h=55
		sdlk.Rectangulo_redondo(screen,dr,10,100,100,100,255)
		
		dr.x=10; dr.y=10; dr.w=sdlk.Ancho-20; dr.h=sdlk.Alto-80
		sdlk.Rectangulo_redondo(screen,dr,10,150,100,150,255)
	def on_imagen1(c:Control)
		sonidos.play("archivo",dd+"soinuak/zenbakiak/"+tostring(3,contador)+".ogg")
		numero.Visible=true
		numero.set_Texto(contador.to_string())
		sdlk.pintar()
		contador++
	
	def on_salir (c:Control)
		sonidos.play("clik")
		fin=true
		comando=-1
	
	def on_continuar (c:Control)
		sonidos.play("clik")
		fin=true
		comando=12
	def on_escucha (c:Control)
		sonidos.play("archivo",dd+"soinuak/zenbakiak/"+tostring(3,solucion)+".ogg")
		
	def on_zuzendu (c:Control)
		var sartuta=0
		for var i=0 to ultimo_de_lista(imgb)
			if sdlk.colision_mascara(imgb[i],casa1)
				sartuta++
				
		if sartuta==solucion
			sonidos.play("ondo")
			fin=true
			comando=12
		else
			sonidos.play("gaizki")

