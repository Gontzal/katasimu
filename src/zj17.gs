uses Gee
uses SDL
uses SDLImage
uses SDLGraphics
uses SDLTTF
uses GLib

// Banan banan doan segida bete

class zenbakiak17 : Object
	numero: list of Etiqueta
	entrada: list of Entrada
	valores: list of int
	fin:bool=false
	comando:int=0
	contador:int=0
	solucion:int=0
	salir:Imagen
	continuar:Imagen
	zuzendu:Boton
	
	init 
		numero= new list of Etiqueta
		entrada= new list of Entrada
		valores= new list of int
	def inicio()
		
		sdlk.muere_todo()
		fin=false
		comando=-10
		contador=1
		
		valores.clear()
		numero.clear()
		entrada.clear()
		
		contador= Random.int_range(1,10)
		
		for var i=0 to 7
			var n= Random.int_range(0,23)
			while valores.contains(n) 
				n= Random.int_range(0,23)
			valores.add(n)
		var x=0
		
		for var i=0 to 7
			numero.add( new Etiqueta(-1,60+i*80,50, 0, 0, contador.to_string()))
			numero[i].set_ColorControl(50,250,90,255)
			numero[i].set_TamanoTexto(50)
			numero[i].Arrastrable=false
			numero[i].Valor_int=contador
			if valores.contains(i)
				numero[i].Visible=false
				entrada.add(new Entrada(-1,(int16)numero[i].Posicion_x,(int16) numero[i].Posicion_y, 60, 75,false))
				entrada[x].set_TamanoTexto(60)
				entrada[x].set_ColorControl(100,230,230,255)
				entrada[x].Valor_int=contador
				x+=1
			contador+=2
			
		
		for var i=8 to 10
			numero.add( new Etiqueta(-1,710,50+(i-8)*80, 0, 0, contador.to_string()))
			numero[i].set_ColorControl(50,250,90,255)
			numero[i].set_TamanoTexto(50)
			numero[i].Arrastrable=false
			numero[i].Valor_int=contador
			if valores.contains(i)
				numero[i].Visible=false
				entrada.add(new Entrada(-1,(int16)numero[i].Posicion_x,(int16) numero[i].Posicion_y, 60, 75,false))
				entrada[x].set_TamanoTexto(60)
				entrada[x].set_ColorControl(100,230,230,255)
				entrada[x].Valor_int=contador
				x+=1
			contador+=2

		for var i=11 to 17 
			numero.add( new Etiqueta(-1,630-(i-11)*80, 210,0, 0, contador.to_string()))
			numero[i].set_ColorControl(50,250,90,255)
			numero[i].set_TamanoTexto(50)
			numero[i].Arrastrable=false
			numero[i].Valor_int=contador
			if valores.contains(i)
				numero[i].Visible=false
				entrada.add(new Entrada(-1,(int16)numero[i].Posicion_x,(int16) numero[i].Posicion_y, 60, 75,false))
				entrada[x].set_TamanoTexto(60)
				entrada[x].set_ColorControl(100,230,230,255)
				entrada[x].Valor_int=contador
				x+=1
			contador+=2
			
		for var i=18 to 19 
			numero.add( new Etiqueta(-1,150,300+(i-18)*80,0, 0, contador.to_string()))
			numero[i].set_ColorControl(50,250,90,255)
			numero[i].set_TamanoTexto(50)
			numero[i].Arrastrable=false
			numero[i].Valor_int=contador
			if valores.contains(i)
				numero[i].Visible=false
				entrada.add(new Entrada(-1,(int16)numero[i].Posicion_x,(int16) numero[i].Posicion_y, 60, 75,false))
				entrada[x].set_TamanoTexto(60)
				entrada[x].set_ColorControl(100,230,230,255)
				entrada[x].Valor_int=contador
				x+=1
			contador+=2

		for var i=20 to 23 
			numero.add( new Etiqueta(-1,220+(i-20)*80,380,0, 0, contador.to_string()))
			numero[i].set_ColorControl(50,250,90,255)
			numero[i].set_TamanoTexto(50)
			numero[i].Arrastrable=false
			numero[i].Valor_int=contador
			if valores.contains(i)
				numero[i].Visible=false
				entrada.add(new Entrada(-1,(int16)numero[i].Posicion_x,(int16) numero[i].Posicion_y, 60, 75,false))
				entrada[x].set_TamanoTexto(60)
				entrada[x].set_ColorControl(100,230,230,255)
				entrada[x].Valor_int=contador
				x+=1
			contador+=2
			
		
		
		
		salir= new Imagen(1,70,547,dd+"irudiak/erabilgarri/atzera.png")
		salir.set_Tamano(40,40)
		salir.izq_pulsado.connect(on_salir)
		
		continuar= new Imagen(1,120,547,dd+"irudiak/erabilgarri/Repetir.png")
		continuar.set_Tamano(40,40)
		continuar.izq_pulsado.connect(on_continuar)
		
		zuzendu= new Boton(1,320,550,0,0,"Zuzendu")
		zuzendu.izq_pulsado.connect(on_zuzendu)
		
		while not fin
			// toma eventos.
			sdlk.mira_sobre()
			while (SDL.Event.poll (out evento))== 1
				if evento.type == SDL.EventType.QUIT
					fin= true
					comando=-1
					break
				else
					sdlk.toma_eventos(evento)
			//pintar el fondo
			pinta_fondo()
			// Realiza los cambios de juego necesarios
			sdlk.update_control()
			// Pinta los controles
			sdlk.pintar()
			SDL.Timer.delay(50)
		//SDL.Timer.delay(500)
		P.continua(comando)
		
	def pinta_fondo()
		Rectangle.fill_rgba(screen, 0, 0, sdlk.Ancho, sdlk.Alto, 255,210,151,255)
		dr:Rect= new Rect
		dr.x=10; dr.y=sdlk.Alto-60; dr.w=sdlk.Ancho-20; dr.h=55
		sdlk.Rectangulo_redondo(screen,dr,10,100,100,100,255)
		
		dr.x=10; dr.y=10; dr.w=sdlk.Ancho-20; dr.h=sdlk.Alto-80
		sdlk.Rectangulo_redondo(screen,dr,10,150,100,150,255)
		
	def on_salir (c:Control)
		sonidos.play("clik")
		fin=true
		comando=-1
	
		
	def on_zuzendu(c:Control)
		var ondo=true
		for var i=0 to ultimo_de_lista(entrada)
			if entrada[i].get_Texto() != entrada[i].Valor_int.to_string()
				ondo=false
				entrada[i].set_ColorControl(250,10,10,255)
			else
				entrada[i].set_ColorControl(100,230,230,255)
		
		if ondo
			sonidos.play("ondo")
			fin=true
			comando=17
		else
			sonidos.play("gaizki")
			
	def on_continuar (c:Control)
		sonidos.play("clik")
		fin=true
		comando=17
		
				

