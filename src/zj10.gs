uses Gee
uses SDL
uses SDLImage
uses SDLGraphics
uses SDLTTF
uses GLib

// zenbakiak idatzi teklatuarekin

class zenbakiak10 : Object
	numero:list of Etiqueta
	numletra:list of Entrada
	ondo:list of Imagen
	
	fin:bool=false
	comando:int=6
	num:int=0
	contador:int=1
	salir:Imagen
	continuar:Imagen
	zuzendu:Boton
	valores: list of int
	init 
		numero= new list of Etiqueta
		numletra= new list of Entrada
		valores= new list of int
		ondo= new list of Imagen

	def inicio()
		sdlk.muere_todo()
		fin=false
		comando=9
		contador=1
		numero.clear()
		numletra.clear()
		valores.clear()
		ondo.clear()
		for var i=0 to 4 
			var num=Random.int_range(1,11)
			while valores.contains(num)
				num=Random.int_range(1,11)
			valores.add(num)
		desordena_lista_int(ref valores)
		
		
		for var i=0 to 3
			numero.add(new Etiqueta(0,100,100*i+50,0,0," "+(valores[i]).to_string()+" " ))
			numero[i].Visible=true
			numero[i].Arrastrable=false
			numero[i].set_ColorControl(200,240,180,255)
			numero[i].set_TamanoTexto(60)
			numero[i].izq_pulsado.connect(on_coger_numero)
			numero[i].Valor_int=valores[i]
		
		for var i=0 to 3
			numletra.add(new Entrada(0,250,100*i+50,300,70,false))
			numletra[i].Visible=true
			numletra[i].set_ColorControl(200,150,200,255)
			numletra[i].set_TamanoTexto(60)
			numletra[i].Valor_str=zenbakiak_n[valores[i]]
		
		for var i=0 to 3
			ondo.add(new Imagen(0,600,100*i+50,dd+"irudiak/erabilgarri/ondo.png"))
			ondo[i].Visible=false
			ondo[i].Arrastrable=false
			ondo[i].set_Tamano(60,60)
			
		salir= new Imagen(1,70,547,dd+"irudiak/erabilgarri/atzera.png")
		salir.set_Tamano(40,40)
		salir.izq_pulsado.connect(on_salir)
		
		continuar= new Imagen(1,120,547,dd+"irudiak/erabilgarri/Repetir.png")
		continuar.set_Tamano(40,40)
		continuar.izq_pulsado.connect(on_continuar)
		
		zuzendu= new Boton(1,320,550,0,0,"Zuzendu")
		zuzendu.izq_pulsado.connect(on_zuzendu)

		
		while not fin
			// toma eventos.
			sdlk.mira_sobre()
			while (SDL.Event.poll (out evento))== 1
				if evento.type == SDL.EventType.QUIT
					fin= true
					comando=-1
					break
				else
					sdlk.toma_eventos(evento)
			//pintar el fondo
			pinta_fondo()		
			// Realiza los cambios de juego necesarios
			sdlk.update_control()
			// Pinta los controles
			sdlk.pintar()
			SDL.Timer.delay(50)
		//SDL.Timer.delay(500)
		P.continua(comando)	
	def pinta_fondo()
		Rectangle.fill_rgba(screen, 0, 0, sdlk.Ancho, sdlk.Alto, 255,210,151,255)
		dr:Rect= new Rect
		dr.x=10; dr.y=sdlk.Alto-60; dr.w=sdlk.Ancho-20; dr.h=55
		sdlk.Rectangulo_redondo(screen,dr,10,100,100,100,255)
		
		dr.x=10; dr.y=10; dr.w=sdlk.Ancho-20; dr.h=sdlk.Alto-80
		sdlk.Rectangulo_redondo(screen,dr,10,150,100,150,255)
	def on_coger_numero(c:Control)
		sonidos.play("archivo",dd+"soinuak/zenbakiak/"+tostring(3,c.Valor_int)+".ogg")
	
	def on_salir (c:Control)
		sonidos.play("clik")
		fin=true
		comando=-1
	
	def on_continuar (c:Control)
		sonidos.play("clik")
		fin=true
		comando=10

	def on_zuzendu (c:Control)
		var ond=true
		for var i=0 to 3
			if numletra[i].get_Texto() != numletra[i].Valor_str
				ond=false
				numletra[i].set_Texto("")
				ondo[i].Visible=false

			else
				ondo[i].Visible=true

		if ond
			sonidos.play("ondo")
			fin=true
			comando=10
		
