uses Gee
uses SDL
uses SDLImage
uses SDLGraphics
uses SDLTTF
uses GLib

// Aurreko eta atzeko zenbakia idatzi.1-50

class zenbakiak24 : Object
	numero: list of Etiqueta
	aurreko: list of Entrada
	atzeko: list of Entrada
	valores: list of int
	fin:bool=false
	comando:int=0
	solucion:int=0
	contador:int=1
	salir:Imagen
	continuar:Imagen
	zuzendu:Boton
	
	init 
		numero= new list of Etiqueta
		aurreko= new list of Entrada
		atzeko= new list of Entrada
		valores= new list of int
	def inicio()
		
		sdlk.muere_todo()
		fin=false
		comando=-10
		contador=1
		
		valores.clear()
		numero.clear()
		aurreko.clear()
		atzeko.clear()
		
		for var i=0 to 4
			var n= Random.int_range(51,100)
			while valores.contains(n)
				n= Random.int_range(51,100)
			valores.add(n)
		
		var etaurre= new Etiqueta(-1,130,10,0,0,"Aurrekoa")
		etaurre.set_TamanoTexto(20)
		etaurre.Arrastrable=false 
		
		var etatze= new Etiqueta(-1,530,10,0,0,"Ondorengoa")
		etatze.set_TamanoTexto(20)
		etatze.Arrastrable=false 
		
		for var i=0 to 4
			numero.add( new Etiqueta(-1,290,60+i*80, 0, 0, "   "+valores[i].to_string()+"   "))
			numero[i].set_ColorControl(100,250,250,255)
			numero[i].set_TamanoTexto(50)
			numero[i].Arrastrable=false
			numero[i].Valor_int=valores[i]
			
			atzeko.add( new Entrada(-1,550,60+i*80, 50, 70 ,false))
			atzeko[i].set_ColorControl(100,200,200,255)
			atzeko[i].set_TamanoTexto(50)
			atzeko[i].Arrastrable=false
			atzeko[i].Valor_int=valores[i]+1
			
			aurreko.add( new Entrada(-1,150,60+i*80, 50, 70 ,false))
			aurreko[i].set_ColorControl(100,200,200,255)
			aurreko[i].set_TamanoTexto(50)
			aurreko[i].Arrastrable=false
			aurreko[i].Valor_int=valores[i]-1
		
		
		
		salir= new Imagen(1,70,547,dd+"irudiak/erabilgarri/atzera.png")
		salir.set_Tamano(40,40)
		salir.izq_pulsado.connect(on_salir)
		
		continuar= new Imagen(1,120,547,dd+"irudiak/erabilgarri/Repetir.png")
		continuar.set_Tamano(40,40)
		continuar.izq_pulsado.connect(on_continuar)
		
		zuzendu= new Boton(1,320,550,0,0,"Zuzendu")
		zuzendu.izq_pulsado.connect(on_zuzendu)
		
		while not fin
			// toma eventos.
			sdlk.mira_sobre()
			while (SDL.Event.poll (out evento))== 1
				if evento.type == SDL.EventType.QUIT
					fin= true
					comando=-1
					break
				else
					sdlk.toma_eventos(evento)
			//pintar el fondo
			pinta_fondo()		
			// Realiza los cambios de juego necesarios
			sdlk.update_control()
			// Pinta los controles
			sdlk.pintar()
			SDL.Timer.delay(50)
		//SDL.Timer.delay(500)
		P.continua(comando)
		
	def pinta_fondo()
		Rectangle.fill_rgba(screen, 0, 0, sdlk.Ancho, sdlk.Alto, 255,210,151,255)
		dr:Rect= new Rect
		dr.x=10; dr.y=sdlk.Alto-60; dr.w=sdlk.Ancho-20; dr.h=55
		sdlk.Rectangulo_redondo(screen,dr,10,100,100,100,255)
		
		dr.x=10; dr.y=10; dr.w=sdlk.Ancho-20; dr.h=sdlk.Alto-80
		sdlk.Rectangulo_redondo(screen,dr,10,150,100,150,255)
		
	def on_salir (c:Control)
		sonidos.play("clik")
		fin=true
		comando=-1
	
		
	def on_zuzendu(c:Control)
		var ondo=true
		for var i=0 to ultimo_de_lista(aurreko)
		
			if aurreko[i].Valor_int.to_string() != aurreko[i].get_Texto()
				aurreko[i].set_ColorControl(255,10,10,255)
				ondo=false
			else
				aurreko[i].set_ColorControl(100,200,200,255)
		
		for var i=0 to ultimo_de_lista(atzeko)
			if atzeko[i].Valor_int.to_string() != atzeko[i].get_Texto()
				atzeko[i].set_ColorControl(255,10,10,255)
				ondo=false
			else
				atzeko[i].set_ColorControl(100,200,200,255)
		if ondo
			sonidos.play("ondo")
			fin=true
			comando=24
		else
			sonidos.play("gaizki")
			
	def on_continuar (c:Control)
		sonidos.play("clik")
		fin=true
		comando=24
		
				

