// valac --disable-warnings --pkg gee-1.0 --pkg sdl --pkg sdl-mixer --pkg sdl-image  --pkg sdl-gfx --pkg sdl-ttf -X -lSDL_ttf  -X -lSDL_gfx -X -lSDL_image -X -lSDL_mixer -o "%e"  cadenas.gs zj00.gs zj01.gs zj02.gs zj03.gs zj04.gs zj05.gs zj06.gs zj07.gs zj08.gs zj09.gs zj10.gs zj11.gs zj12.gs  zj13.gs zj14.gs zj15.gs zj16.gs zj17.gs zj18.gs zj19.gs zj20.gs ../sdlk/sdlk2.gs  "%f" --Xcc=-I/usr/include/SDL
// La biblioteca sdlk2 se encuentra un nivel antes del directorio de katamotz-zenbakiak puesto que debe de tener un desarrollo diferenciado con el programa.
// Hasiera 2013ko Apirilaren 5a.


uses Gee
uses SDL
uses SDLImage
uses SDLGraphics
uses SDLTTF
uses GLib

// general
sdlk:Sdlk
imagen:Imagen
screen: unowned Screen
ancho:int16=800
alto:int16=600
evento: SDL.Event
P:principal
puntos:int=0
dd:string
zenbakiak_s: dict of string,int
zenbakiak_n: dict of int,string
iruditrans: list of string
j00: zenbakiak00;j01: zenbakiak01; j02:zenbakiak02; j03:zenbakiak03;j04:zenbakiak04;
j05: zenbakiak05;j06: zenbakiak06; j07:zenbakiak07; j08:zenbakiak08; j09:zenbakiak09;
j10: zenbakiak10; j11: zenbakiak11; j12:zenbakiak12; j13:zenbakiak13; j14:zenbakiak14;
j15: zenbakiak15; j16: zenbakiak16; j17:zenbakiak17; j18:zenbakiak18; j19:zenbakiak19;
j20: zenbakiak20; j21: zenbakiak21; j22:zenbakiak22; j23:zenbakiak23; j24:zenbakiak24;
j25: zenbakiak25; j26: zenbakiak26; j27:zenbakiak27; //j23:zenbakiak23; j24:zenbakiak24;

puntu:int=0
sonidos:Sonidos
init
	//dd="/home/gontzal/Mahaigaina/programazioa/katasimu/katasimu-1.0/"
	dd="/usr/share/katasimu/"
	j00= new zenbakiak00 (); j01= new zenbakiak01 (); j02= new zenbakiak02 (); j03= new zenbakiak03 (); j04= new zenbakiak04 ();
	j05= new zenbakiak05 (); j06= new zenbakiak06 (); j07= new zenbakiak07 (); j08= new zenbakiak08 (); j09= new zenbakiak09 ();
	j10= new zenbakiak10 (); j11= new zenbakiak11 (); j12= new zenbakiak12 (); j13= new zenbakiak13 (); j14= new zenbakiak14 ();
	j15= new zenbakiak15 (); j16= new zenbakiak16 (); j17= new zenbakiak17 (); j18= new zenbakiak18 (); j19= new zenbakiak19 ();
	j20= new zenbakiak20 (); j21= new zenbakiak21 (); j22= new zenbakiak22 (); j23= new zenbakiak23 (); j24= new zenbakiak24 ();
	j25= new zenbakiak25 (); j26= new zenbakiak26 (); j27= new zenbakiak27 (); //j23= new zenbakiak23 (); j24= new zenbakiak24 ();
	SDL.init()
	SDLTTF.init()// SurfaceFlag.FULLSCREEN |
	
	screen = SDL.Screen.set_video_mode (ancho, alto,25,SurfaceFlag.DOUBLEBUF | SurfaceFlag.HWACCEL | SurfaceFlag.HWSURFACE)
	var a= SDLMixer.open(44100,SDL.AudioFormat.S16LSB,2,4096)
	//
	SDL.Key.set_repeat (50,50)
	SDL.Key.enable_unicode (1)
	
	sdlk= new Sdlk(ancho,alto)
	
	//SDL.Timer.delay(1000)
	sonidos= new Sonidos()
	
	//abrimos para coger los nombres
	var f = FileStream.open(dd+"datuak/zenbakiak","r")
	var c=""
	zenbakiak_s= new dict of string,int
	zenbakiak_n= new dict of int,string
	c=f.read_line()
	
	while not f.eof()
		var datos = c.split (",") 
		zenbakiak_s[datos[0]]=int.parse(datos[1])
		zenbakiak_n[int.parse(datos[1])]=(datos[0])
		c=f.read_line()
		
	iruditrans= new list of string
	try
		var name="" 
		d:GLib.Dir = Dir.open( dd+"irudiak/hitz_transparenteak" )
		while ( (name = d.read_name()) != null) 
			iruditrans.add(name)
	except
		print"no pudo leerse el directorio"

	
	P= new principal() // menu principal
	P.inicio()
	
	
	
class principal:Object
	img: Imagen
	img2: Imagen
	aukera: list of Imagen
	aukera2: list of Boton
	interr:Interruptor
	atzera:Imagen
	selec:Selector
	aa:list of string
	en:Entrada
	activado:bool=false
	tiempo:int=0
	fin:bool=false
	comando:int=-1
	fin_total:bool=false
	sr1:Rect
	dr1:Rect
	video1:Video
	video2:Video
	init 
		aukera= new list of Imagen
		aukera2= new list of Boton
		
	def inicio()
		fin=false
		comando=-1
		sdlk.muere_todo()
		aukera.clear()
		aukera2.clear()
		aukera.add(new Imagen (100,100,50,dd+"irudiak/erabilgarri/0-5.png"))
		aukera.add(new Imagen (200,375,50,dd+"irudiak/erabilgarri/0-10.png"))
		aukera.add(new Imagen (300,100,275,dd+"irudiak/erabilgarri/0-50.png"))
		aukera.add(new Imagen (400,375,275,dd+"irudiak/erabilgarri/0-100.png"))
		
		for var i=0 to ultimo_de_lista(aukera)
			aukera[i].izq_pulsado.connect (on_boton)
			aukera[i].set_Tamano(200,200)
			aukera[i].Resaltar_Sobre=true
			
		atzera= new Imagen(0,300,20,dd+"irudiak/erabilgarri/atzera.png")
		atzera.Visible=false
		atzera.set_Tamano(100,100)
		atzera.izq_pulsado.connect(on_atzera)
		atzera.Arrastrable=false
		
		/*
		video1= new Video (0,350,350,160,120,dd+"irudiak/hizki_video/s_hizkia")
		video1.Arrastrable=true
		video1.izq_pulsado.connect(on_video1)
		
		video2= new Video (0,350,350,160,120,dd+"irudiak/hizki_video/r_hizkia")
		video2.Arrastrable=true
		video2.izq_pulsado.connect(on_video2)
		*/
		while not fin
			// toma eventos.
			sdlk.mira_sobre()
			while (SDL.Event.poll (out evento))== 1
				case evento.type
					when EventType.KEYDOWN
						if  (SDL.Key.get_name(evento.key.keysym.sym)=="escape")
							fin=true
							break
					when SDL.EventType.QUIT
						fin= true
						comando=-1
						break
					default
						sdlk.toma_eventos(evento)
			
			//pintar el fondo
			pinta_fondo()
			// Pinta los controles
			sdlk.pintar()
			sdlk.update_control()
			SDL.Timer.delay(50)
			
		case comando
			when -2,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29  // aldaketa gehitzean
				continua(comando)
			default 
				SDL.quit()
	
	def pinta_fondo()
		Rectangle.fill_rgba(screen, 0, 0, sdlk.Ancho, sdlk.Alto, 255,210,151,255)
		
	def on_video1(c:Control)
		video1.play()
	def on_video2(c:Control)
		video2.play()
	
	def continua(c:int)
		case c    // aldaketa gehitzean
			when 0 do j00.inicio(); when 1 do j01.inicio(); when 2 do j02.inicio(); when 3 do j03.inicio();
			when 4 do j04.inicio(); when 5 do j05.inicio(); when 6 do j06.inicio(); when 7 do j07.inicio();
			when 8 do j08.inicio(); when 9 do j09.inicio(); when 10 do	j10.inicio();
			when 11 do j11.inicio(); when 12 do	j12.inicio(); when 13 do j13.inicio(); 	when 14 do	j14.inicio();
			when 15 do j15.inicio(); when 16 do	j16.inicio(); 
			when 17 do j17.inicio(); when 18 do j18.inicio(); when 19 do j19.inicio(); when 20 do j20.inicio(); 
			when 21 do j21.inicio(); when 22 do j22.inicio();when 23 do j23.inicio(); when 24 do j24.inicio(); 
			when 25 do j25.inicio(); when 26 do j26.inicio(); when 27 do j27.inicio(); //when 24 do j24.inicio(); 
			when -1,-2
				P.inicio()
			
	def on_aukera2 (c:Control)
		case c.Ref  // aldaketa gehitzean
			when 1000
				fin=true; comando=0
			when 1001
				fin=true; comando=1	
			when 1002
				fin=true; comando=2
			when 1003
				fin=true; comando=3
			when 1004
				fin=true; comando=4
			when 1005
				fin=true; comando=5
			when 1006
				fin=true; comando=6
			when 2000
				fin=true; comando=7
			when 2001
				fin=true; comando=8
			when 2002
				fin=true; comando=9
			when 2003
				fin=true; comando=10
			when 2004
				fin=true; comando=11
			when 2005
				fin=true; comando=12
			when 3000 
				fin=true; comando=13
			when 3001
				fin=true; comando=14
			when 3002
				fin=true; comando=15
			when 3003
				fin=true; comando=16
			when 3004 
				fin=true; comando=17
			when 3005
				fin=true; comando=18
			when 3006
				fin=true; comando=19
			when 3007
				fin=true; comando=20
			when 4000
				fin=true; comando=21
			when 4001
				fin=true; comando=22
			when 4002
				fin=true; comando=23
			when 4003
				fin=true; comando=24
			when 4004
				fin=true; comando=25
			when 4005
				fin=true; comando=26
			when 4006
				fin=true; comando=27

	def on_atzera(c:Control)
		fin=true; comando=-2;
		
	def on_boton(c:Control)
		for var i=0 to (ultimo_de_lista(aukera))
			aukera[i].Valor_bool=false
			aukera[i].Visible=false
		atzera.Visible=true
		c.Valor_bool=true

		case c.Ref
		
			when 100
				for var i=0 to ultimo_de_lista(aukera2) do	aukera2[i].muere()
				aukera2.clear()
				strbot2: array of string = {"Objektuak klikatu eta zenbakia entzun.",
				"Berdindu ezkerreko objektu kopurua.","Zenbakia ikusi eta irudi kopuru bera lortu.",
				"Irudi kopurua adierazi zenbakiz.","Entzun eta click egin puxtarrian.","Espazio untziaren jokoa.","Zenbakiak ordenatu."}
				for var i=0 to (strbot2.length-1)
					aukera2.add (new Boton(c.Ref*10+i,150,i*60+150,0,0,strbot2[i]) )
					aukera2[i].set_TamanoTexto(30)
					aukera2[i].izq_pulsado.connect(on_aukera2)

			when 200
				for var i=0 to ultimo_de_lista(aukera2) do aukera2[i].muere()
				aukera2.clear()
				strbot2: array of string = {"Aukeratu zenbaki egokia.",
				"Entzundako zenbakia lotu.","Zenbakiak ordenatu","Idatzi zenbakiak.","Zenbatu objektuak eta hautatu zenbaki egokia.",
				"Entzun zenbakia eta egokitu objektu kopurua."}
				for var i=0 to (strbot2.length-1)
					aukera2.add (new Boton(c.Ref*10+i,150,i*60+150,0,0,strbot2[i]))
					aukera2[i].set_TamanoTexto(30)
					aukera2[i].izq_pulsado.connect(on_aukera2)
					
			when 300
				for var i=0 to ultimo_de_lista(aukera2) do aukera2[i].muere()
				aukera2.clear()
				strbot2: array of string = {"Kokatu zenbakiak taulan.",
				"Hamarrekoak identifikatu erregleta erabiliz.","Aurreko eta atzeko zenbakia idatzi.","Idatzi falta diren zenbakiak:banan-banan.",
				"Idatzi falta diren zenbakiak:binan binan.","Entzun zenbakia eta hautatu.","Irakurri eta aukeratu zenbaki egokia.","Erregletak kokatu zenbakiaren arabera."}
				for var i=0 to (strbot2.length-1)
					aukera2.add (new Boton(c.Ref*10+i,150,i*40+150,0,0,strbot2[i]))
					aukera2[i].set_TamanoTexto(20)
					aukera2[i].izq_pulsado.connect(on_aukera2)
				
			when 400
				for var i=0 to ultimo_de_lista(aukera2) do aukera2[i].muere()
				aukera2.clear()
				strbot2: array of string = {"Entzun zenbakiak eta kokatu.","Ordenatu zenbakiak","Hautatu zenbakiak taulan",
				"Aurreko eta atzeko zenbakiak idatzi","Irakurri eta aukeratu zenbaki egokia","Erregletak kokatu zenbakiaren arabera","Zenbaki egokia aukeratu"}
				for var i=0 to (strbot2.length-1)
					aukera2.add (new Boton(c.Ref*10+i,150,i*40+150,0,0,strbot2[i]))
					aukera2[i].set_TamanoTexto(20)
					aukera2[i].izq_pulsado.connect(on_aukera2)
				
				
class Sonidos: Object
	archivo: SDL.RWops
	archivo2: SDL.RWops
	aclik: SDL.RWops
	clik:SDLMixer.Chunk
	botella: SDLMixer.Chunk 
	gaizki: SDLMixer.Chunk 
	aondo0: SDL.RWops
	ondo0: SDLMixer.Chunk 
	aondo1: SDL.RWops
	ondo1: SDLMixer.Chunk 
	aondo2: SDL.RWops
	ondo2: SDLMixer.Chunk 
	aondo3: SDL.RWops
	ondo3: SDLMixer.Chunk 
	aondo4: SDL.RWops
	ondo4: SDLMixer.Chunk 
	
	aondo5: SDL.RWops
	ondo5: SDLMixer.Chunk 
	
	canal: SDLMixer.Channel
	musica: SDLMixer.Music
	init
		archivo= new SDL.RWops.from_file (dd+"soinuak/zaratak/botella.ogg","rb")
		botella= new SDLMixer.Chunk.WAV (archivo,-1)
		archivo2= new SDL.RWops.from_file (dd+"soinuak/zaratak/boing.ogg","rb")
		gaizki= new SDLMixer.Chunk.WAV (archivo2,-1)
		aondo0= new SDL.RWops.from_file (dd+"soinuak/zaratak/ondo0.ogg","rb")
		ondo0= new SDLMixer.Chunk.WAV (aondo0,-1)
		aondo1= new SDL.RWops.from_file (dd+"soinuak/zaratak/ondo1.ogg","rb")
		ondo1= new SDLMixer.Chunk.WAV (aondo1,-1)
		aondo2= new SDL.RWops.from_file (dd+"soinuak/zaratak/ondo2.ogg","rb")
		ondo2= new SDLMixer.Chunk.WAV (aondo2,-1)
		aondo3= new SDL.RWops.from_file (dd+"soinuak/zaratak/ondo3.ogg","rb")
		ondo3= new SDLMixer.Chunk.WAV (aondo3,-1)
		aondo4= new SDL.RWops.from_file (dd+"soinuak/zaratak/ondo4.ogg","rb")
		ondo4= new SDLMixer.Chunk.WAV (aondo4,-1)
		aondo5= new SDL.RWops.from_file (dd+"soinuak/zaratak/ondo5.ogg","rb")
		ondo5= new SDLMixer.Chunk.WAV (aondo5,-1)
		aclik= new SDL.RWops.from_file (dd+"soinuak/zaratak/click.ogg","rb")
		clik= new SDLMixer.Chunk.WAV (aclik,-1)
		
	def play (s:string,archivo:string="")
		case s
			when "blub"
				canal.play(botella,0)
			when "clik"
				canal.play(clik,0)
			when "gaizki"
				canal.play(gaizki,0)
				
			when "ondo"
				var n=Random.int_range(0,6)
				case n
					when 0
						canal.play(ondo0,0)
					when 1
						canal.play(ondo1,0)
					when 2
						canal.play(ondo2,0)
					when 3
						canal.play(ondo3,0)
					when 4
						canal.play(ondo4,0)
					when 5
						canal.play(ondo5,0)
			when "archivo"
				musica= new SDLMixer.Music (archivo)
				musica.play(1)
				
