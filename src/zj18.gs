uses Gee
uses SDL
uses SDLImage
uses SDLGraphics
uses SDLTTF
uses GLib


class zenbakiak18 : Object
	iman: list of CuadradoRedondo
	numero:list of Etiqueta
	numletra:list of Imagen
	fin:bool=false
	comando:int=6
	num:int=0
	contador:int=1
	salir:Imagen
	continuar:Imagen
	zuzendu:Boton
	valores: list of int
	init 
		iman= new list of CuadradoRedondo
		numero= new list of Etiqueta
		numletra= new list of Imagen
		valores= new list of int
	def inicio()
		sdlk.muere_todo()
		fin=false
		comando=6
		contador=1
		iman.clear()
		numero.clear()
		valores.clear()
		numletra.clear()
		
		for var i=0 to 4 
			valores.add(Random.int_range(1,6)*10)
		desordena_lista_int(ref valores)
		
		for var i=0 to 4
			iman.add( new CuadradoRedondo (1, 300, 35+(100*i),70,70))
			iman[i].Arrastrable=false
			iman[i].Valor_int=-1
			iman[i].set_ColorControl(100,250,200,255)
		
		for var i=0 to 4
			numero.add(new Etiqueta(0,500,30+(i*100),0,0," "+(valores[i]).to_string()+" " ))
			numero[i].Visible=true
			numero[i].set_ColorControl(100,150,(uchar)Random.int_range(100,200),255)
			numero[i].set_TamanoTexto(60)
			numero[i].Arrastrable=true
			numero[i].soltado.connect(on_soltar_numero)
			numero[i].izq_pulsado.connect(on_coger_numero)
			numero[i].Valor_int=valores[i]
			
		desordena_lista_int(ref valores)
		for var i=0 to 4
			numletra.add(new Imagen(0,150,i*100+30, dd+"irudiak/erabilgarri/musika.png"))
			numletra[i].Visible=true
			numletra[i].Arrastrable=false
			numletra[i].set_Tamano(80,80)
			numletra[i].Valor_int=valores[i]
			numletra[i].izq_pulsado.connect(on_imagen_sonido)
			
			
		
		salir= new Imagen(1,70,547,dd+"irudiak/erabilgarri/atzera.png")
		salir.set_Tamano(40,40)
		salir.izq_pulsado.connect(on_salir)
		
		continuar= new Imagen(1,120,547,dd+"irudiak/erabilgarri/Repetir.png")
		continuar.set_Tamano(40,40)
		continuar.izq_pulsado.connect(on_continuar)
		
		zuzendu= new Boton(1,320,550,0,0,"Zuzendu")
		zuzendu.izq_pulsado.connect(on_zuzendu)

		var ev=false
		while not fin
			// toma eventos.
			ev=false
			while (SDL.Event.poll (out evento))== 1
				if evento.type == SDL.EventType.QUIT
					fin= true
					comando=-1
					break
				else
					sdlk.toma_eventos(evento)
					ev=true
			//pintar el fondo
			pinta_fondo()
			// Realiza los cambios de juego necesarios
			sdlk.mira_sobre()
			sdlk.update_control()
			// Pinta los controles
			sdlk.pintar()
			SDL.Timer.delay(50)
		//SDL.Timer.delay(500)
		P.continua(comando)	
	
	def pinta_fondo()
		Rectangle.fill_rgba(screen, 0, 0, sdlk.Ancho, sdlk.Alto, 255,210,151,255)
		dr:Rect= new Rect
		dr.x=10; dr.y=sdlk.Alto-60; dr.w=sdlk.Ancho-20; dr.h=55
		sdlk.Rectangulo_redondo(screen,dr,10,100,100,100,255)
		
		dr.x=10; dr.y=10; dr.w=sdlk.Ancho-20; dr.h=sdlk.Alto-80
		sdlk.Rectangulo_redondo(screen,dr,10,150,100,150,255)
		
		
	def on_soltar_numero (c:Control)
		for var i=0 to 4
			if sdlk.colision_cuadrada(iman[i],c) and iman[i].Valor_int<0
				iman[i].Valor_int = c.Valor_int
				sonidos.play("blub")
				c.set_Posicion_x((iman[i].dr.x+iman[i].dr.w/2)-(c.dr.w/2))
				c.set_Posicion_y((iman[i].dr.y+iman[i].dr.h/2)-(c.dr.h/2))
	
	def on_coger_numero(c:Control)
		for var i=0 to 4
			if sdlk.colision_cuadrada(iman[i],c) and (c.Valor_int==iman[i].Valor_int) 
				iman[i].Valor_int=-1
			else
				sonidos.play("archivo",dd+"soinuak/zenbakiak/"+tostring(3,c.Valor_int)+".ogg")
	
	
	def on_imagen_sonido (c:Control)
		sonidos.play("archivo",dd+"soinuak/zenbakiak/"+tostring(3,c.Valor_int)+".ogg")
		
	def on_salir (c:Control)
		sonidos.play("clik")
		fin=true
		comando=-1
	
	def on_continuar (c:Control)
		sonidos.play("clik")
		fin=true
		comando=18
		//SDL.WindowManager.toggle_fullscreen(screen)
		//Process.spawn_command_line_sync ("mplayer -fs /home/gontzal/capture.avi")
		//SDL.WindowManager.toggle_fullscreen(screen)
		

	def on_zuzendu (c:Control)
		var ondo=true
		for var i=0 to 4
			if (iman[i].Valor_int!=numletra[i].Valor_int) or (iman[i].Valor_int < 0)
				ondo=false
				
		if ondo
			sonidos.play("ondo")
			
			fin=true
			comando=18
		else
			sonidos.play("gaizki")
