uses Gee
uses SDL
uses SDLImage
uses SDLGraphics
uses SDLTTF
uses GLib


class zenbakiak05 : Object
	numero:Etiqueta
	etb: list of Etiqueta
	valores: list of int
	fin:bool=false
	comando:int=0
	solucion:int=0
	contador:int=1
	salir:Imagen
	continuar:Imagen
	zuzendu:Boton
	tempo:int=0
	nave:Imagen
	suzkobola:Imagen
	disparar:bool=false
	avanza:double=0
	parar:bool=false
	
	init 
		etb= new list of Etiqueta
		valores= new list of int
	
	def inicio()
		
		sdlk.muere_todo()
		fin=false
		comando=-10
		contador=1
		tempo=0
		solucion= Random.int_range(1,5)
		
		etb.clear()
		valores.clear()
		
		var archivo= iruditrans [ Random.int_range(0,ultimo_de_lista(iruditrans)) ]
		
		
		// Esta es la zona donde las letras rebotan
		nave= new Imagen(0,60,450,dd+"irudiak/erabilgarri/nave.png")
		nave.Arrastrable=false
		nave.set_Tamano(70,40)
		nave.Arrastrable=false
		//El disparo
		
		suzkobola= new Imagen(0,60,450,dd+"irudiak/erabilgarri/suzkobola.png")
		suzkobola.Arrastrable=true
		suzkobola.set_Tamano(40,40)
		suzkobola.Visible=false
		suzkobola.update.connect(on_suzkobola)
		disparar=false
		
		//Estas son las etiquetas de las letas
		
		
		for var i=0 to 7
			var numeroazar= Random.int_range(1,5)
			etb.add( new Etiqueta(i,i*80+40,30,0,0," "+(numeroazar).to_string()+" "))
			etb[i].Valor_int=numeroazar
			etb[i].set_ColorControl(250,(uchar)Random.int_range(10,254),(uchar)Random.int_range(10,254),255)
			etb[i].set_TamanoTexto(45)
			etb[i].Arrastrable=false
			etb[i].update.connect (on_mover_numero)
			valores.add(numeroazar)
		
		numero= new Etiqueta(1,nave.get_Posicion_x()+25,500,0,0,"  ")
		numero.Arrastrable=false
		
		salir= new Imagen(1,70,547,dd+"irudiak/erabilgarri/atzera.png")
		salir.set_Tamano(40,40)
		salir.izq_pulsado.connect(on_salir)
		
		continuar= new Imagen(1,120,547,dd+"irudiak/erabilgarri/Repetir.png")
		continuar.set_Tamano(40,40)
		continuar.izq_pulsado.connect(on_continuar)
		
		crea_numero()
		sdlk.tecleado.disconnect(on_teclado)
		sdlk.tecleado.connect(on_teclado)
		
		while not fin
			// toma eventos.
			sdlk.mira_sobre()
			while (SDL.Event.poll (out evento))== 1
				if evento.type == SDL.EventType.QUIT
					fin= true
					comando=-1
					break
				else
					sdlk.toma_eventos(evento)
			//pintar el fondo
			pinta_fondo()		
			// Realiza los cambios de juego necesarios
			sdlk.update_control()
			// Pinta los controles
			sdlk.pintar()
			SDL.Timer.delay(50)
			
		//SDL.Timer.delay(500)
		P.continua(comando)
	
	def pinta_fondo()
		Rectangle.fill_rgba(screen, 0, 0, sdlk.Ancho, sdlk.Alto, 255,210,151,255)
		dr:Rect= new Rect
		dr.x=10; dr.y=sdlk.Alto-60; dr.w=sdlk.Ancho-20; dr.h=55
		sdlk.Rectangulo_redondo(screen,dr,10,100,100,100,255)
		
		dr.x=10; dr.y=10; dr.w=sdlk.Ancho-20; dr.h=sdlk.Alto-80
		sdlk.Rectangulo_redondo(screen,dr,10,150,100,150,255)
	/////////////////////////////////////////////////////////////////////////////////
	def on_suzkobola()
		if disparar==true
			var n=suzkobola.get_Posicion_y()-(20)
			suzkobola.set_Posicion_y(n)
			
			if n<10
				disparar=false
				suzkobola.Visible=false
			else
				for var i=0 to ultimo_de_lista(etb)
					var c=etb[i]
					if sdlk.colision_cuadrada(c,suzkobola)
						if solucion==c.Valor_int
							sonidos.play("blub")
							c.Visible=false
							suzkobola.Visible=false
							disparar=false
							valores.remove_at(valores.index_of(solucion))
							if not valores.contains(solucion)
								sonidos.play("ondo")
								fin=true
								comando=5
								avanza+=0.2
		
						else
							sonidos.play("gaizki")
							disparar=false
							suzkobola.Visible=false
					
	def on_teclado(s:string)
		case s
			when "left"
				var x=nave.get_Posicion_x()-20
				nave.set_Posicion_x(x)
				numero.set_Posicion_x(x+25)
				
			when "right"
				var x=nave.get_Posicion_x()+20
				nave.set_Posicion_x(x)
				numero.set_Posicion_x(x+25)
			when "space"
				if disparar==false
					suzkobola.Visible=true
					suzkobola.set_Posicion_x(nave.get_Posicion_x()+20)
					suzkobola.set_Posicion_y(400)
					disparar=true
				
	def on_mover_numero (c:Control)		
		if not parar
			c.set_Posicion_y ( c.get_Posicion_y() + 1+(int)avanza )
			if sdlk.colision_cuadrada(nave,c)
				sonidos.play("gaizki")
				nave.Visible=false
				parar=true
				avanza=0
			
	
	def crea_numero()
		var n=0
		if ultimo_de_lista(valores)>0
			n=Random.int_range (0,ultimo_de_lista(valores))
		else 
			n=0
		solucion=valores[n]
		numero.set_Texto(solucion.to_string())
		sonidos.play("archivo",dd+"soinuak/zenbakiak/"+tostring(3,solucion)+".ogg")
		

	
	
	def on_salir (c:Control)
		sonidos.play("clik")
		fin=true
		comando=-1
	
	def on_continuar (c:Control)
		sonidos.play("clik")
		fin=true
		comando=5
		parar=false
	
		
