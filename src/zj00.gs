uses Gee
uses SDL
uses SDLImage
uses SDLGraphics
uses SDLTTF
uses GLib


class zenbakiak00 : Object
	imga: list of Imagen
	numero:Etiqueta
	fin:bool=false
	comando:int=0
	num:int=0
	contador:int=1
	salir:Imagen
	continuar:Imagen
	init 
		imga= new list of Imagen
		
	def inicio()
		sdlk.muere_todo()
		fin=false
		comando=-1
		contador=1
		num= Random.int_range(1,5)
		imga.clear()
		var archivo= iruditrans [ Random.int_range(0,ultimo_de_lista(iruditrans)) ]
		for var i=0 to num
			imga.add( new Imagen (1, 100,10+i*100,dd+"irudiak/hitz_transparenteak/"+archivo))
			imga[i].set_Tamano (100,100)
			imga[i].izq_pulsado.connect(this.on_imagen1)
		
		numero= new Etiqueta(0,400,70,0,0,"-")
		numero.Visible=false
		numero.set_ColorControl(100,150,100,255)
		numero.set_TamanoTexto(360)
		numero.Arrastrable=false
		
		salir= new Imagen(1,70,547,dd+"irudiak/erabilgarri/atzera.png")
		salir.set_Tamano(40,40)
		salir.izq_pulsado.connect(on_salir)
		
		continuar= new Imagen(1,120,547,dd+"irudiak/erabilgarri/Repetir.png")
		continuar.set_Tamano(40,40)
		continuar.izq_pulsado.connect(on_continuar)
		
		while not fin
			// toma eventos.
			
			sdlk.mira_sobre()
			while (SDL.Event.poll (out evento))== 1
				if evento.type == SDL.EventType.QUIT
					fin= true
					comando=-1
					break
				else
					sdlk.toma_eventos(evento)
					
			// Realiza los cambios de juego necesarios
			sdlk.update_control()
			//pintar el fondo
			pinta_fondo()
			// Pinta los controles
			sdlk.pintar()
			SDL.Timer.delay(50)
		P.continua(comando)	
	
	def pinta_fondo()
		Rectangle.fill_rgba(screen, 0, 0, sdlk.Ancho, sdlk.Alto, 255,210,151,255)
		dr:Rect= new Rect
		dr.x=10; dr.y=sdlk.Alto-60; dr.w=sdlk.Ancho-20; dr.h=55
		sdlk.Rectangulo_redondo(screen,dr,10,100,100,100,255)
		
		dr.x=10; dr.y=10; dr.w=sdlk.Ancho-20; dr.h=sdlk.Alto-80
		sdlk.Rectangulo_redondo(screen,dr,10,100,200,100,255)
		
		
	def on_imagen1(c:Control)
		c.Visible=false
		sonidos.play("archivo",dd+"soinuak/zenbakiak/"+tostring(3,contador)+".ogg")
		numero.Visible=true
		numero.set_Texto(contador.to_string())
		
		sdlk.pintar()
		if contador>num
			//SDL.Timer.delay(500)
			sonidos.play("ondo")
			fin=true
			comando=0
		else
			contador++
	
	def on_salir (c:Control)
		sonidos.play("clik")
		fin=true
		comando=-1
		
	
	def on_continuar (c:Control)
		sonidos.play("clik")
		fin=true
		comando=0
