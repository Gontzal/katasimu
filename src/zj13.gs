uses Gee
uses SDL
uses SDLImage
uses SDLGraphics
uses SDLTTF
uses GLib

// Entzun zenbakia eta egokitu kopurua.1-50

class zenbakiak13 : Object
	numeros: list of Etiqueta
	tabla: list of Boton
	valores:list of int
	fin:bool=false
	comando:int=0
	solucion:int=0
	contador:int=1
	salir:Imagen
	continuar:Imagen
	zuzendu:Boton
	
	init 
		numeros= new list of Etiqueta
		tabla= new list of Boton
		valores= new list of int
	def inicio()
		
		sdlk.muere_todo()
		fin=false
		comando=-10
		contador=1
		tabla.clear()
		numeros.clear()
		valores.clear()
		for var i=0 to 5
			var n= Random.int_range(1,50)
			while valores.contains(n)
				n= Random.int_range(1,50)
			valores.add(n)

		
		for var i=0 to 4
			for var j=0 to 9
				tabla.add( new Boton(i*10+j,40+70*j,20+80*i, 0, 0, (i*10+j).to_string()))
				tabla[i*10+j].set_ColorControl(100,0,100,255)
				tabla[i*10+j].set_TamanoTexto(40)
				tabla[i*10+j].Arrastrable=false
				tabla[i*10+j].izq_pulsado.connect (on_tabla)
		
		
		for var i=0 to 5
			numeros.add( new Etiqueta(-1,100+100*i,440, 0, 0, valores[i].to_string()))
			numeros[i].set_ColorControl(100,250,250,255)
			numeros[i].set_TamanoTexto(45)
			numeros[i].Arrastrable=false
			numeros[i].Valor_int=valores[i]
			
		
		salir= new Imagen(1,70,547,dd+"irudiak/erabilgarri/atzera.png")
		salir.set_Tamano(40,40)
		salir.izq_pulsado.connect(on_salir)
		
		continuar= new Imagen(1,120,547,dd+"irudiak/erabilgarri/Repetir.png")
		continuar.set_Tamano(40,40)
		continuar.izq_pulsado.connect(on_continuar)
		
		
		while not fin
			// toma eventos.
			sdlk.mira_sobre()
			while (SDL.Event.poll (out evento))== 1
				if evento.type == SDL.EventType.QUIT
					fin= true
					comando=-1
					break
				else
					sdlk.toma_eventos(evento)
			//pintar el fondo
			pinta_fondo()		
			// Realiza los cambios de juego necesarios
			sdlk.update_control()
			// Pinta los controles
			sdlk.pintar()
			SDL.Timer.delay(50)
		
		//SDL.Timer.delay(500)
		P.continua(comando)
	
	def pinta_fondo()
		Rectangle.fill_rgba(screen, 0, 0, sdlk.Ancho, sdlk.Alto, 255,210,151,255)
		dr:Rect= new Rect
		dr.x=10; dr.y=sdlk.Alto-60; dr.w=sdlk.Ancho-20; dr.h=55
		sdlk.Rectangulo_redondo(screen,dr,10,100,100,100,255)
		
		dr.x=10; dr.y=10; dr.w=sdlk.Ancho-20; dr.h=sdlk.Alto-80
		sdlk.Rectangulo_redondo(screen,dr,10,150,100,150,255)
				
	def on_imagen1(c:Control)
		sonidos.play("archivo",dd+"soinuak/zenbakiak/"+tostring(3,contador)+".ogg")
		sdlk.pintar()
		contador++
	
	def on_salir (c:Control)
		sonidos.play("clik")
		fin=true
		comando=-1
	
	def on_tabla(c:Control)
			
		if valores.contains(c.Ref)
			valores.remove_at(valores.index_of(c.Ref))
			for var i=0 to 5
				if numeros[i].Valor_int==c.Ref
					c.Visible=false
					numeros[i].Visible=false
					sonidos.play("archivo",dd+"soinuak/zenbakiak/"+tostring(3,numeros[i].Valor_int)+".ogg")
					sdlk.pintar()
		else
			sonidos.play("gaizki")
			
		if ultimo_de_lista(valores)==-1
			SDL.Timer.delay(1200)
			sonidos.play("ondo")
			fin=true
			comando=13
		
			
		
	def on_continuar (c:Control)
		sonidos.play("clik")
		fin=true
		comando=13
		
				

