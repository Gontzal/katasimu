uses Gee
uses SDL
uses SDLImage
uses SDLGraphics
uses SDLTTF
uses GLib


class zenbakiak11 : Object
	imga: list of Imagen
	imgb: list of Imagen
	numero:Etiqueta
	casa0:CuadradoRedondo
	etb: list of Etiqueta
	casa1:CuadradoRedondo
	igual:Imagen
	fin:bool=false
	comando:int=0
	solucion:int=0
	contador:int=1
	salir:Imagen
	continuar:Imagen
	zuzendu:Boton
	
	init 
		imga= new list of Imagen
		imgb= new list of Imagen
		etb= new list of Etiqueta
		
	def inicio()
		
		sdlk.muere_todo()
		fin=false
		comando=-10
		contador=1
		solucion= Random.int_range(1,11)
		imga.clear()
		etb.clear()
		var archivo= iruditrans [ Random.int_range(0,ultimo_de_lista(iruditrans)) ]
		
		casa0= new CuadradoRedondo(0,80,30,160,480)
		casa0.set_ColorControl(100,150,150,255)
		casa0.Arrastrable=false
		casa0.FiguraFondo=true
		
		casa1= new CuadradoRedondo(0,400,170,160,160)
		casa1.set_ColorControl(100,150,150,255)
		casa1.Arrastrable=false
		casa1.FiguraFondo=true
		casa1.Valor_int=-1
		
		
		igual= new Imagen(0,250,200,dd+"irudiak/erabilgarri/berdin.png")
		igual.Arrastrable=false
		
		for var i=0 to (solucion-1)
			imga.add( new Imagen (1, 130,30+i*45,dd+"irudiak/hitz_transparenteak/"+archivo))
			imga[i].set_Tamano (50,50)
			imga[i].izq_pulsado.connect(this.on_imagen1)
			imga[i].Arrastrable=false
		
		for var i=0 to 9
			etb.add( new Etiqueta(0,700,20+i*50,0,0,(i+1).to_string()))
			etb[i].Valor_int=i+1
			etb[i].set_ColorControl(200,230,150,255)
			etb[i].set_TamanoTexto(27)
			etb[i].Arrastrable=true
			etb[i].soltado.connect (on_soltar_numero)
			etb[i].izq_pulsado.connect (on_coger_numero)
		
		
		numero= new Etiqueta(0,310,70,0,0," ")
		numero.Visible=false
		numero.set_ColorControl(100,150,100,255)
		numero.set_TamanoTexto(60)
		numero.Arrastrable=false
		
		
		salir= new Imagen(1,70,547,dd+"irudiak/erabilgarri/atzera.png")
		salir.set_Tamano(40,40)
		salir.izq_pulsado.connect(on_salir)
		
		continuar= new Imagen(1,120,547,dd+"irudiak/erabilgarri/Repetir.png")
		continuar.set_Tamano(40,40)
		continuar.izq_pulsado.connect(on_continuar)
		
		zuzendu= new Boton(1,320,550,0,0,"Zuzendu")
		zuzendu.izq_pulsado.connect(on_zuzendu)
		
		while not fin
			// toma eventos.
			sdlk.mira_sobre()
			while (SDL.Event.poll (out evento))== 1
				if evento.type == SDL.EventType.QUIT
					fin= true
					comando=-1
					break
				else
					sdlk.toma_eventos(evento)
			//pintar el fondo
			pinta_fondo()		
			// Realiza los cambios de juego necesarios
			sdlk.update_control()
			// Pinta los controles
			sdlk.pintar()
			SDL.Timer.delay(50)
		
		//SDL.Timer.delay(500)
		P.continua(comando)
		
	def pinta_fondo()
		Rectangle.fill_rgba(screen, 0, 0, sdlk.Ancho, sdlk.Alto, 255,210,151,255)
		dr:Rect= new Rect
		dr.x=10; dr.y=sdlk.Alto-60; dr.w=sdlk.Ancho-20; dr.h=55
		sdlk.Rectangulo_redondo(screen,dr,10,100,100,100,255)
		
		dr.x=10; dr.y=10; dr.w=sdlk.Ancho-20; dr.h=sdlk.Alto-80
		sdlk.Rectangulo_redondo(screen,dr,10,150,100,150,255)		
	def on_soltar_numero (c:Control)
		if sdlk.colision_mascara(casa1,c) and casa1.Valor_int<0
			casa1.Valor_int=c.Valor_int
			sonidos.play("blub")
			c.set_Posicion_x((casa1.dr.x+casa1.dr.w/2)-(c.dr.w/2))
			c.set_Posicion_y((casa1.dr.y+casa1.dr.h/2)-(c.dr.h/2))
			
	def on_coger_numero(c:Control)
		if sdlk.colision_mascara(casa1,c) and (c.Valor_int==casa1.Valor_int) 
			casa1.Valor_int=-1
		else
			sonidos.play("archivo",dd+"soinuak/zenbakiak/"+tostring(3,c.Valor_int)+".ogg")
		
	
	def on_imagen1(c:Control)
		sonidos.play("archivo",dd+"soinuak/zenbakiak/"+zenbakiak_n[contador]+".ogg")
		numero.Visible=true
		numero.set_Texto(contador.to_string())
		sdlk.pintar()
		contador++
	
	def on_salir (c:Control)
		sonidos.play("clik")
		fin=true
		comando=-1
	
	def on_continuar (c:Control)
		sonidos.play("clik")
		fin=true
		comando=11
	
	def on_zuzendu (c:Control)
				
		if casa1.Valor_int==solucion
			sonidos.play("ondo")
			fin=true
			comando=11
		else
			sonidos.play("gaizki")

