uses Gee
uses SDL
uses SDLImage
uses SDLGraphics
uses SDLTTF
uses GLib


class zenbakiak04 : Object
	numero:Etiqueta
	casa0:Etiqueta
	etx: list of int
	ety: list of int
	etb: list of Etiqueta
	valores: list of int
	casa1:Etiqueta
	zona: CuadradoRedondo
	fin:bool=false
	comando:int=0
	solucion:int=0
	contador:int=1
	salir:Imagen
	continuar:Imagen
	zuzendu:Boton
	tempo:int=0
	init 
		etb= new list of Etiqueta
		etx= new list of int
		valores= new list of int
	
	def inicio()
		
		sdlk.muere_todo()
		fin=false
		comando=-10
		contador=1
		tempo=0
		solucion= Random.int_range(1,5)
		
		etx.clear()
		ety.clear()
		etb.clear()
		valores.clear()
		
		var archivo= iruditrans [ Random.int_range(0,ultimo_de_lista(iruditrans)) ]
		
		
		// Esta es la zona donde las letras rebotan
		zona= new CuadradoRedondo(0,20,20,600,400)
		zona.set_ColorControl(100,150,150,255)
		zona.Arrastrable=false
		zona.FiguraFondo=true
		
		//Estas son las etiquetas de los numeros
		
		for var i=0 to 10
			var numeroazar= Random.int_range(1,5)
			etb.add( new Etiqueta(i,Random.int_range(100,500),Random.int_range(50,300),0,0,(numeroazar).to_string()))
			
			etb[i].Valor_int=numeroazar
			etb[i].set_ColorControl(250,(uchar)Random.int_range(10,254),(uchar)Random.int_range(10,254),255)
			etb[i].Borde=false
			etb[i].set_TamanoTexto(Random.int_range(30,100))
			etb[i].Arrastrable=false
			etb[i].izq_pulsado.connect (on_coger_numero)
			etb[i].update.connect (on_mover_numero)
			
			
			etx.add(Random.int_range(2,5))
			ety.add(Random.int_range(2,5))
			valores.add(numeroazar)
		
		salir= new Imagen(1,70,547,dd+"irudiak/erabilgarri/atzera.png")
		salir.set_Tamano(40,40)
		salir.izq_pulsado.connect(on_salir)
		
		continuar= new Imagen(1,120,547,dd+"irudiak/erabilgarri/Repetir.png")
		continuar.set_Tamano(40,40)
		continuar.izq_pulsado.connect(on_continuar)
		
		crea_numero()
		
		
		while not fin
			// toma eventos.
			sdlk.mira_sobre()
			while (SDL.Event.poll (out evento))== 1
				if evento.type == SDL.EventType.QUIT
					fin= true
					comando=-1
					break
				else
					sdlk.toma_eventos(evento)
			
			//pintar el fondo
			pinta_fondo()
			// Realiza los cambios de juego necesarios
			sdlk.update_control()
			// Pinta los controles
			sdlk.pintar()
			SDL.Timer.delay(50)
		
		//SDL.Timer.delay(500)
		P.continua(comando)

	def pinta_fondo()
		Rectangle.fill_rgba(screen, 0, 0, sdlk.Ancho, sdlk.Alto, 255,210,151,255)
		dr:Rect= new Rect
		dr.x=10; dr.y=sdlk.Alto-60; dr.w=sdlk.Ancho-20; dr.h=55
		sdlk.Rectangulo_redondo(screen,dr,10,100,100,100,255)
		
		dr.x=10; dr.y=10; dr.w=sdlk.Ancho-20; dr.h=sdlk.Alto-80
		sdlk.Rectangulo_redondo(screen,dr,10,150,100,150,255)

	def crea_numero()
		var n=0
		if ultimo_de_lista(valores)>0
			n=Random.int_range (0,ultimo_de_lista(valores))
		else 
			n=0
		solucion=valores[n]
		sonidos.play("archivo",dd+"soinuak/zenbakiak/"+tostring(3,solucion)+".ogg")
		
	def on_mover_numero (c:Control)
		tempo++
		if tempo>500
			crea_numero()
			tempo=0
			
		c.set_Posicion_x ( c.get_Posicion_x() + (int)etx[c.Ref] )
		c.set_Posicion_y ( c.get_Posicion_y() + (int)ety[c.Ref] )
		
		if c.get_Posicion_x() < zona.get_Posicion_x()
			etx[c.Ref]=etx[c.Ref] *(-1)
			
		if c.get_Posicion_y() < zona.get_Posicion_y()
			ety[c.Ref]=ety[c.Ref] *(-1)
			
		if c.get_Posicion_x()+c.get_Ancho() > zona.get_Posicion_x()+zona.get_Ancho()
			etx[c.Ref]=etx[c.Ref] *(-1)
			
		if c.get_Posicion_y()+c.get_Alto() > zona.get_Posicion_y()+zona.get_Alto()
			ety[c.Ref]=ety[c.Ref] *(-1)
		
		
		
	def on_coger_numero(c:Control)
		if c.Valor_int==solucion
			c.muere()
			valores.remove_at(valores.index_of(solucion))
			if ultimo_de_lista(valores)<0
				fin=true
				comando=4
				SDL.Timer.delay(20)
				sonidos.play("ondo")
			
			else
				SDL.Timer.delay(20)
				sonidos.play("blub")
				tempo=0
				//SDL.Timer.delay(500)
				crea_numero()
				
		else
			sonidos.play("gaizki")


	
	
	def on_salir (c:Control)
		sonidos.play("clik")
		fin=true
		comando=-1
	
	def on_continuar (c:Control)
		sonidos.play("clik")
		fin=true
		comando=4
	
		
